<h1>DRIXT - REDUX CONFIG - v1.0.17</h1>

This is a base Redux config based on our experience using it on day to day projects.

The setup prepare a basic config using React, Redux, ReduxThunk, Superagent, Reactotron, etc, for use on our projects.

Some useful scripts are:

- ReduxStore: the principal piece of this module, starts the ReduxStore;

- BaseRATypes: Base Redux Action Types;

- BaseActions: Contains the basic actions, like asyncStart, asyncEnd, baseAsyncThunk
	
- BaseReducer: Base reducer to support the BaseActions;
