"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = useRegisterLazyReducer;
var _react = _interopRequireWildcard(require("react"));
var _index = require("../index");
function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }
function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }
// Hook

/**
 * React Root to Register Lazy Reducers on the Hooks way to provide an easy way to do in-component reducer
 * initialization
 *
 * @param reducerName - The name of reducer
 * @param reducer - The reducer function reference
 * @returns {boolean} - The state of lazy registration.
 */
function useRegisterLazyReducer(reducerName, reducer) {
  var _useState = (0, _react.useState)(false),
    ready = _useState[0],
    setReady = _useState[1];
  (0, _react.useEffect)(function () {
    if (!_index.ReduxStore.getStore().getState()[reducerName]) {
      _index.ReduxStore.registerLazyReducer(reducerName, reducer);
      setTimeout(function () {
        setReady(true);
      }, 100);
    } else {
      setReady(true);
    }
  }, []);
  return ready;
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6WyJ1c2VSZWdpc3RlckxhenlSZWR1Y2VyIiwicmVkdWNlck5hbWUiLCJyZWR1Y2VyIiwidXNlU3RhdGUiLCJyZWFkeSIsInNldFJlYWR5IiwidXNlRWZmZWN0IiwiUmVkdXhTdG9yZSIsImdldFN0b3JlIiwiZ2V0U3RhdGUiLCJyZWdpc3RlckxhenlSZWR1Y2VyIiwic2V0VGltZW91dCJdLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9yZWR1eC91c2VSZWdpc3RlckxhenlSZWR1Y2VyLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8vIEhvb2tcbmltcG9ydCBSZWFjdCwge3VzZUVmZmVjdCwgdXNlU3RhdGV9IGZyb20gXCJyZWFjdFwiO1xuaW1wb3J0IHtSZWR1eFN0b3JlfSBmcm9tIFwiLi4vaW5kZXhcIjtcblxuLyoqXG4gKiBSZWFjdCBSb290IHRvIFJlZ2lzdGVyIExhenkgUmVkdWNlcnMgb24gdGhlIEhvb2tzIHdheSB0byBwcm92aWRlIGFuIGVhc3kgd2F5IHRvIGRvIGluLWNvbXBvbmVudCByZWR1Y2VyXG4gKiBpbml0aWFsaXphdGlvblxuICpcbiAqIEBwYXJhbSByZWR1Y2VyTmFtZSAtIFRoZSBuYW1lIG9mIHJlZHVjZXJcbiAqIEBwYXJhbSByZWR1Y2VyIC0gVGhlIHJlZHVjZXIgZnVuY3Rpb24gcmVmZXJlbmNlXG4gKiBAcmV0dXJucyB7Ym9vbGVhbn0gLSBUaGUgc3RhdGUgb2YgbGF6eSByZWdpc3RyYXRpb24uXG4gKi9cbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIHVzZVJlZ2lzdGVyTGF6eVJlZHVjZXIocmVkdWNlck5hbWUsIHJlZHVjZXIpIHtcbiAgIGNvbnN0IFtyZWFkeSwgc2V0UmVhZHldID0gdXNlU3RhdGUoZmFsc2UpO1xuXG4gICB1c2VFZmZlY3QoKCkgPT4ge1xuICAgICAgaWYgKCFSZWR1eFN0b3JlLmdldFN0b3JlKCkuZ2V0U3RhdGUoKVtyZWR1Y2VyTmFtZV0pIHtcbiAgICAgICAgIFJlZHV4U3RvcmUucmVnaXN0ZXJMYXp5UmVkdWNlcihyZWR1Y2VyTmFtZSwgcmVkdWNlcik7XG5cbiAgICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgICAgICAgc2V0UmVhZHkodHJ1ZSk7XG4gICAgICAgICB9LCAxMDApO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgIHNldFJlYWR5KHRydWUpO1xuICAgICAgfVxuICAgfSwgW10pO1xuXG4gICByZXR1cm4gcmVhZHk7XG59XG4iXSwibWFwcGluZ3MiOiI7Ozs7OztBQUNBO0FBQ0E7QUFBb0M7QUFBQTtBQUZwQzs7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ2UsU0FBU0Esc0JBQXNCLENBQUNDLFdBQVcsRUFBRUMsT0FBTyxFQUFFO0VBQ2xFLGdCQUEwQixJQUFBQyxlQUFRLEVBQUMsS0FBSyxDQUFDO0lBQWxDQyxLQUFLO0lBQUVDLFFBQVE7RUFFdEIsSUFBQUMsZ0JBQVMsRUFBQyxZQUFNO0lBQ2IsSUFBSSxDQUFDQyxpQkFBVSxDQUFDQyxRQUFRLEVBQUUsQ0FBQ0MsUUFBUSxFQUFFLENBQUNSLFdBQVcsQ0FBQyxFQUFFO01BQ2pETSxpQkFBVSxDQUFDRyxtQkFBbUIsQ0FBQ1QsV0FBVyxFQUFFQyxPQUFPLENBQUM7TUFFcERTLFVBQVUsQ0FBQyxZQUFNO1FBQ2ROLFFBQVEsQ0FBQyxJQUFJLENBQUM7TUFDakIsQ0FBQyxFQUFFLEdBQUcsQ0FBQztJQUNWLENBQUMsTUFBTTtNQUNKQSxRQUFRLENBQUMsSUFBSSxDQUFDO0lBQ2pCO0VBQ0gsQ0FBQyxFQUFFLEVBQUUsQ0FBQztFQUVOLE9BQU9ELEtBQUs7QUFDZiJ9