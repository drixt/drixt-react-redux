/**
 * React Root to Register Lazy Reducers on the Hooks way to provide an easy way to do in-component reducer
 * initialization
 *
 * @param reducerName - The name of reducer
 * @param reducer - The reducer function reference
 * @returns {boolean} - The state of lazy registration.
 */
export default function useRegisterLazyReducer(reducerName: any, reducer: any): boolean;
