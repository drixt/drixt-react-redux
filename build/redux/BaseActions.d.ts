/**
 * Action creator that start a Async Request Action.
 * @param subtype
 * @param httpRequest
 * @returns {{subtype: string, httpRequest: null, type: string}}
 */
export function asyncStart(subtype?: string, httpRequest?: any): {
    subtype: string;
    httpRequest: null;
    type: string;
};
/**
 * Action creator that start a Async Request Action.
 * @param subtype
 * @returns {{subtype: string, type: string}}
 */
export function asyncEnd(subtype?: string): {
    subtype: string;
    type: string;
};
/**
 * Fires a CLEAR action to a specific reducer and reducer value.
 * @param reducer - the reducer name
 * @param dataTarget - field name on reducer, can be a array of field names
 * @param cleanValue - OPTIONAL - value to use on 'cleaning' process.
 * Ex. undefined, 0, "", etc.
 * Defaults to null
 */
export function clearReducerData(reducer: any, dataTarget: any, cleanValue?: any): {
    type: string;
    reducer: any;
    dataTarget: any;
    cleanValue: any;
};
/**
 * Base Async Thunk for common use of async redux actions! ;D
 * @param restApiRequest - The promise returned by the Rest API Path Call
 * @param actionType - Then Redux Action Type to hit Reducer
 * @param customResponseTransform - A function to transform the response body results. Default: returns res.body
 * @param postActionCallback - OPTIONAL: A function to call a custom behavior after the thunk job
 * @returns Function Thunk
 */
export function baseAsyncThunk(restApiRequest: any, actionType: any, customResponseTransform?: (res: any, body: any, hasError: any) => any, postActionCallback?: any): (dispatch: any) => void;
declare namespace _default {
    export { asyncStart };
    export { asyncEnd };
    export { baseAsyncThunk };
    export { clearReducerData };
}
export default _default;
