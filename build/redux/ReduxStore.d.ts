/**
 * Deep clones a JSON state
 * @param currState
 */
export function deepClone(currState: any): any;
declare namespace _default {
    export { init };
    export { getStore };
    export { registerLazyReducer };
}
export default _default;
/**
 * The principal method of this file.
 * Starts the store.
 *
 * @param middlewares
 * @param enhancers
 */
declare function init(middlewares?: any[], enhancers?: any[]): any;
/**
 * Return the current value of reduxStore
 *
 * @returns {reduxStore}
 */
declare function getStore(): any;
/**
 * Utility Function to register Reducers on the fly.
 * @param name
 * @param reducer
 */
declare function registerLazyReducer(name: any, reducer: any): void;
