export default function BaseReducer(state: {
    isFetching: boolean;
    fetchingActions: any[];
    fetchingHttpRequests: any[];
}, action: any): {
    isFetching: boolean;
    fetchingActions: any[];
    fetchingHttpRequests: any[];
};
