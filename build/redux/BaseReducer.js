"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = BaseReducer;
var _BaseRATypes = require("./BaseRATypes");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
/**
 * Base reducer supporting the base actions
 */

var defaultState = {
  isFetching: false,
  //isLoading
  fetchingActions: [],
  fetchingHttpRequests: []
};
function BaseReducer(state, action) {
  if (state === void 0) {
    state = defaultState;
  }
  switch (action.type) {
    case _BaseRATypes.ASYNC_START:
      return _objectSpread(_objectSpread({}, state), {}, {
        isFetching: true,
        fetchingActions: [].concat(state.fetchingActions, [action.subtype]),
        fetchingHttpRequests: [].concat(state.fetchingHttpRequests, [action.httpRequest])
      });
    case _BaseRATypes.ASYNC_END:
      var index = state.fetchingActions.indexOf(action.subtype);
      var fetchingActions = [].concat(state.fetchingActions);
      var fetchingHttpRequests = [].concat(state.fetchingHttpRequests);
      fetchingActions.splice(index, 1);
      fetchingHttpRequests.splice(index, 1);
      return _objectSpread(_objectSpread({}, state), {}, {
        isFetching: fetchingActions.length !== 0,
        fetchingActions: fetchingActions,
        fetchingHttpRequests: fetchingHttpRequests
      });
    default:
      return state;
  }
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6WyJkZWZhdWx0U3RhdGUiLCJpc0ZldGNoaW5nIiwiZmV0Y2hpbmdBY3Rpb25zIiwiZmV0Y2hpbmdIdHRwUmVxdWVzdHMiLCJCYXNlUmVkdWNlciIsInN0YXRlIiwiYWN0aW9uIiwidHlwZSIsIkFTWU5DX1NUQVJUIiwic3VidHlwZSIsImh0dHBSZXF1ZXN0IiwiQVNZTkNfRU5EIiwiaW5kZXgiLCJpbmRleE9mIiwic3BsaWNlIiwibGVuZ3RoIl0sInNvdXJjZXMiOlsiLi4vLi4vc3JjL3JlZHV4L0Jhc2VSZWR1Y2VyLmpzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7QVNZTkNfRU5ELCBBU1lOQ19TVEFSVCx9IGZyb20gJy4vQmFzZVJBVHlwZXMnO1xuXG4vKipcbiAqIEJhc2UgcmVkdWNlciBzdXBwb3J0aW5nIHRoZSBiYXNlIGFjdGlvbnNcbiAqL1xuXG5jb25zdCBkZWZhdWx0U3RhdGUgPSB7XG5cdGlzRmV0Y2hpbmc6IGZhbHNlLCAvL2lzTG9hZGluZ1xuXHRmZXRjaGluZ0FjdGlvbnM6IFtdLFxuXHRmZXRjaGluZ0h0dHBSZXF1ZXN0czogW10sXG59O1xuXG5cbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIEJhc2VSZWR1Y2VyKHN0YXRlID0gZGVmYXVsdFN0YXRlLCBhY3Rpb24pIHtcblx0c3dpdGNoIChhY3Rpb24udHlwZSkge1xuXHRcdGNhc2UgQVNZTkNfU1RBUlQ6XG5cdFx0XHRyZXR1cm4ge1xuXHRcdFx0XHQuLi5zdGF0ZSxcblx0XHRcdFx0aXNGZXRjaGluZzogdHJ1ZSxcblx0XHRcdFx0ZmV0Y2hpbmdBY3Rpb25zOiBbLi4uc3RhdGUuZmV0Y2hpbmdBY3Rpb25zLCBhY3Rpb24uc3VidHlwZV0sXG5cdFx0XHRcdGZldGNoaW5nSHR0cFJlcXVlc3RzOiBbLi4uc3RhdGUuZmV0Y2hpbmdIdHRwUmVxdWVzdHMsIGFjdGlvbi5odHRwUmVxdWVzdF0sXG5cdFx0XHR9O1xuXG5cdFx0Y2FzZSBBU1lOQ19FTkQ6XG5cdFx0XHRsZXQgaW5kZXggPSBzdGF0ZS5mZXRjaGluZ0FjdGlvbnMuaW5kZXhPZihhY3Rpb24uc3VidHlwZSk7XG5cdFx0XHRsZXQgZmV0Y2hpbmdBY3Rpb25zID0gWy4uLnN0YXRlLmZldGNoaW5nQWN0aW9uc107XG5cdFx0XHRsZXQgZmV0Y2hpbmdIdHRwUmVxdWVzdHMgPSBbLi4uc3RhdGUuZmV0Y2hpbmdIdHRwUmVxdWVzdHNdO1xuXG5cdFx0XHRmZXRjaGluZ0FjdGlvbnMuc3BsaWNlKGluZGV4LCAxKTtcblx0XHRcdGZldGNoaW5nSHR0cFJlcXVlc3RzLnNwbGljZShpbmRleCwgMSk7XG5cblx0XHRcdHJldHVybiB7XG5cdFx0XHRcdC4uLnN0YXRlLFxuXHRcdFx0XHRpc0ZldGNoaW5nOiBmZXRjaGluZ0FjdGlvbnMubGVuZ3RoICE9PSAwLFxuXHRcdFx0XHRmZXRjaGluZ0FjdGlvbnMsXG5cdFx0XHRcdGZldGNoaW5nSHR0cFJlcXVlc3RzLFxuXHRcdFx0fTtcblxuXHRcdGRlZmF1bHQ6XG5cdFx0XHRyZXR1cm4gc3RhdGVcblx0fVxufVxuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQTtBQUFzRDtBQUFBO0FBQUE7QUFFdEQ7QUFDQTtBQUNBOztBQUVBLElBQU1BLFlBQVksR0FBRztFQUNwQkMsVUFBVSxFQUFFLEtBQUs7RUFBRTtFQUNuQkMsZUFBZSxFQUFFLEVBQUU7RUFDbkJDLG9CQUFvQixFQUFFO0FBQ3ZCLENBQUM7QUFHYyxTQUFTQyxXQUFXLENBQUNDLEtBQUssRUFBaUJDLE1BQU0sRUFBRTtFQUFBLElBQTlCRCxLQUFLO0lBQUxBLEtBQUssR0FBR0wsWUFBWTtFQUFBO0VBQ3ZELFFBQVFNLE1BQU0sQ0FBQ0MsSUFBSTtJQUNsQixLQUFLQyx3QkFBVztNQUNmLHVDQUNJSCxLQUFLO1FBQ1JKLFVBQVUsRUFBRSxJQUFJO1FBQ2hCQyxlQUFlLFlBQU1HLEtBQUssQ0FBQ0gsZUFBZSxHQUFFSSxNQUFNLENBQUNHLE9BQU8sRUFBQztRQUMzRE4sb0JBQW9CLFlBQU1FLEtBQUssQ0FBQ0Ysb0JBQW9CLEdBQUVHLE1BQU0sQ0FBQ0ksV0FBVztNQUFDO0lBRzNFLEtBQUtDLHNCQUFTO01BQ2IsSUFBSUMsS0FBSyxHQUFHUCxLQUFLLENBQUNILGVBQWUsQ0FBQ1csT0FBTyxDQUFDUCxNQUFNLENBQUNHLE9BQU8sQ0FBQztNQUN6RCxJQUFJUCxlQUFlLGFBQU9HLEtBQUssQ0FBQ0gsZUFBZSxDQUFDO01BQ2hELElBQUlDLG9CQUFvQixhQUFPRSxLQUFLLENBQUNGLG9CQUFvQixDQUFDO01BRTFERCxlQUFlLENBQUNZLE1BQU0sQ0FBQ0YsS0FBSyxFQUFFLENBQUMsQ0FBQztNQUNoQ1Qsb0JBQW9CLENBQUNXLE1BQU0sQ0FBQ0YsS0FBSyxFQUFFLENBQUMsQ0FBQztNQUVyQyx1Q0FDSVAsS0FBSztRQUNSSixVQUFVLEVBQUVDLGVBQWUsQ0FBQ2EsTUFBTSxLQUFLLENBQUM7UUFDeENiLGVBQWUsRUFBZkEsZUFBZTtRQUNmQyxvQkFBb0IsRUFBcEJBO01BQW9CO0lBR3RCO01BQ0MsT0FBT0UsS0FBSztFQUFBO0FBRWYifQ==