"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _BaseRATypes = require("./BaseRATypes");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * Base reducer supporting the base actions
 */
var defaultState = {
  isFetching: false,
  fetchingActions: [],
  fetchingHttpRequests: []
};

var _default = function _default(state, action) {
  if (state === void 0) {
    state = defaultState;
  }

  switch (action.type) {
    case _BaseRATypes.ASYNC_START:
      return _objectSpread(_objectSpread({}, state), {}, {
        isFetching: true,
        fetchingActions: [].concat(state.fetchingActions, [action.subtype]),
        fetchingHttpRequests: [].concat(state.fetchingHttpRequests, [action.httpRequest])
      });

    case _BaseRATypes.ASYNC_END:
      var index = state.fetchingActions.indexOf(action.subtype);
      var fetchingActions = [].concat(state.fetchingActions);
      var fetchingHttpRequests = [].concat(state.fetchingHttpRequests);
      fetchingActions.splice(index, 1);
      fetchingHttpRequests.splice(index, 1);
      return _objectSpread(_objectSpread({}, state), {}, {
        isFetching: fetchingActions.length !== 0,
        fetchingActions: fetchingActions,
        fetchingHttpRequests: fetchingHttpRequests
      });

    default:
      return state;
  }
};

exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9yZWR1eC9CYXNlUmVkdWNlci5qcyJdLCJuYW1lcyI6WyJkZWZhdWx0U3RhdGUiLCJpc0ZldGNoaW5nIiwiZmV0Y2hpbmdBY3Rpb25zIiwiZmV0Y2hpbmdIdHRwUmVxdWVzdHMiLCJzdGF0ZSIsImFjdGlvbiIsInR5cGUiLCJBU1lOQ19TVEFSVCIsInN1YnR5cGUiLCJodHRwUmVxdWVzdCIsIkFTWU5DX0VORCIsImluZGV4IiwiaW5kZXhPZiIsInNwbGljZSIsImxlbmd0aCJdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUFBOzs7Ozs7OztBQUVBO0FBQ0E7QUFDQTtBQUVBLElBQU1BLFlBQVksR0FBRztBQUNwQkMsRUFBQUEsVUFBVSxFQUFFLEtBRFE7QUFFcEJDLEVBQUFBLGVBQWUsRUFBRSxFQUZHO0FBR3BCQyxFQUFBQSxvQkFBb0IsRUFBRTtBQUhGLENBQXJCOztlQU1lLGtCQUFDQyxLQUFELEVBQXVCQyxNQUF2QixFQUFrQztBQUFBLE1BQWpDRCxLQUFpQztBQUFqQ0EsSUFBQUEsS0FBaUMsR0FBekJKLFlBQXlCO0FBQUE7O0FBQ2hELFVBQVFLLE1BQU0sQ0FBQ0MsSUFBZjtBQUNDLFNBQUtDLHdCQUFMO0FBQ0MsNkNBQ0lILEtBREo7QUFFQ0gsUUFBQUEsVUFBVSxFQUFFLElBRmI7QUFHQ0MsUUFBQUEsZUFBZSxZQUFNRSxLQUFLLENBQUNGLGVBQVosR0FBNkJHLE1BQU0sQ0FBQ0csT0FBcEMsRUFIaEI7QUFJQ0wsUUFBQUEsb0JBQW9CLFlBQU1DLEtBQUssQ0FBQ0Qsb0JBQVosR0FBa0NFLE1BQU0sQ0FBQ0ksV0FBekM7QUFKckI7O0FBT0QsU0FBS0Msc0JBQUw7QUFDQyxVQUFJQyxLQUFLLEdBQUdQLEtBQUssQ0FBQ0YsZUFBTixDQUFzQlUsT0FBdEIsQ0FBOEJQLE1BQU0sQ0FBQ0csT0FBckMsQ0FBWjtBQUNBLFVBQUlOLGVBQWUsYUFBT0UsS0FBSyxDQUFDRixlQUFiLENBQW5CO0FBQ0EsVUFBSUMsb0JBQW9CLGFBQU9DLEtBQUssQ0FBQ0Qsb0JBQWIsQ0FBeEI7QUFFQUQsTUFBQUEsZUFBZSxDQUFDVyxNQUFoQixDQUF1QkYsS0FBdkIsRUFBOEIsQ0FBOUI7QUFDQVIsTUFBQUEsb0JBQW9CLENBQUNVLE1BQXJCLENBQTRCRixLQUE1QixFQUFtQyxDQUFuQztBQUVBLDZDQUNJUCxLQURKO0FBRUNILFFBQUFBLFVBQVUsRUFBRUMsZUFBZSxDQUFDWSxNQUFoQixLQUEyQixDQUZ4QztBQUdDWixRQUFBQSxlQUFlLEVBQWZBLGVBSEQ7QUFJQ0MsUUFBQUEsb0JBQW9CLEVBQXBCQTtBQUpEOztBQU9EO0FBQ0MsYUFBT0MsS0FBUDtBQXpCRjtBQTJCQSxDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtBU1lOQ19FTkQsIEFTWU5DX1NUQVJULH0gZnJvbSAnLi9CYXNlUkFUeXBlcyc7XG5cbi8qKlxuICogQmFzZSByZWR1Y2VyIHN1cHBvcnRpbmcgdGhlIGJhc2UgYWN0aW9uc1xuICovXG5cbmNvbnN0IGRlZmF1bHRTdGF0ZSA9IHtcblx0aXNGZXRjaGluZzogZmFsc2UsXG5cdGZldGNoaW5nQWN0aW9uczogW10sXG5cdGZldGNoaW5nSHR0cFJlcXVlc3RzOiBbXSxcbn07XG5cbmV4cG9ydCBkZWZhdWx0IChzdGF0ZSA9IGRlZmF1bHRTdGF0ZSwgYWN0aW9uKSA9PiB7XG5cdHN3aXRjaCAoYWN0aW9uLnR5cGUpIHtcblx0XHRjYXNlIEFTWU5DX1NUQVJUOlxuXHRcdFx0cmV0dXJuIHtcblx0XHRcdFx0Li4uc3RhdGUsXG5cdFx0XHRcdGlzRmV0Y2hpbmc6IHRydWUsXG5cdFx0XHRcdGZldGNoaW5nQWN0aW9uczogWy4uLnN0YXRlLmZldGNoaW5nQWN0aW9ucywgYWN0aW9uLnN1YnR5cGVdLFxuXHRcdFx0XHRmZXRjaGluZ0h0dHBSZXF1ZXN0czogWy4uLnN0YXRlLmZldGNoaW5nSHR0cFJlcXVlc3RzLCBhY3Rpb24uaHR0cFJlcXVlc3RdLFxuXHRcdFx0fTtcblxuXHRcdGNhc2UgQVNZTkNfRU5EOlxuXHRcdFx0bGV0IGluZGV4ID0gc3RhdGUuZmV0Y2hpbmdBY3Rpb25zLmluZGV4T2YoYWN0aW9uLnN1YnR5cGUpO1xuXHRcdFx0bGV0IGZldGNoaW5nQWN0aW9ucyA9IFsuLi5zdGF0ZS5mZXRjaGluZ0FjdGlvbnNdO1xuXHRcdFx0bGV0IGZldGNoaW5nSHR0cFJlcXVlc3RzID0gWy4uLnN0YXRlLmZldGNoaW5nSHR0cFJlcXVlc3RzXTtcblxuXHRcdFx0ZmV0Y2hpbmdBY3Rpb25zLnNwbGljZShpbmRleCwgMSk7XG5cdFx0XHRmZXRjaGluZ0h0dHBSZXF1ZXN0cy5zcGxpY2UoaW5kZXgsIDEpO1xuXG5cdFx0XHRyZXR1cm4ge1xuXHRcdFx0XHQuLi5zdGF0ZSxcblx0XHRcdFx0aXNGZXRjaGluZzogZmV0Y2hpbmdBY3Rpb25zLmxlbmd0aCAhPT0gMCxcblx0XHRcdFx0ZmV0Y2hpbmdBY3Rpb25zLFxuXHRcdFx0XHRmZXRjaGluZ0h0dHBSZXF1ZXN0cyxcblx0XHRcdH07XG5cblx0XHRkZWZhdWx0OlxuXHRcdFx0cmV0dXJuIHN0YXRlXG5cdH1cbn1cbiJdfQ==