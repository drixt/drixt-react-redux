"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.deepClone = deepClone;
exports.default = void 0;
var _redux = require("redux");
var _reduxThunk = _interopRequireDefault(require("redux-thunk"));
var _BaseReducer = _interopRequireDefault(require("./BaseReducer"));
var _BaseRATypes = require("./BaseRATypes");
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
/**
 * Creates the Redux Store with a default base reducer
 */

var defaultReducers = {
  base: _BaseReducer.default
};

/**
 * References to store
 */
var wasInit = false;
var reduxStore = null;

/**
 * Deep clones a JSON state
 * @param currState
 */
function deepClone(currState) {
  return JSON.parse(JSON.stringify(currState));
}

/**
 * Utility function that combines reducers enabling global catch of CLEAR_REDUCER_INFO
 * @param asyncReducers
 */
function combineReducersWithSugar(asyncReducers) {
  if (asyncReducers === void 0) {
    asyncReducers = {};
  }
  return function (state, action) {
    var nextState = state;
    if (action.type === _BaseRATypes.CLEAR_REDUCER_INFO) {
      if (nextState[action.reducer]) {
        if (!Array.isArray(action.dataTarget)) {
          action.dataTarget = [action.dataTarget];
        }
        action.dataTarget.forEach(function (field) {
          if (nextState[action.reducer].hasOwnProperty(field)) {
            try {
              nextState[action.reducer][field] = action.cleanValue;
            } catch (e) {
              console.error("[CLEAR_REDUCER_DATA] The given reducer '" + action.reducer + "' can't allow state change from the generic 'CLEAR_REDUCER_DATA' action. If you are using immer or something else, maybe you should evict this approach and create custom clean data actions.");
              console.error("[CLEAR_REDUCER_DATA]", e);
            }
          } else {
            console.error("[CLEAR_REDUCER_DATA] The given reducer '" + action.reducer + "' don't has a defined property '" + field + "'. Current existing keys are: " + Object.keys(nextState[action.reducer]));
          }
        });
      } else {
        console.error("[CLEAR_REDUCER_DATA] The given reducer '" + action.reducer + "' doesn't exists. Current existing reducers are: " + Object.keys(nextState));
      }
    }
    return (0, _redux.combineReducers)(_objectSpread(_objectSpread({}, defaultReducers), asyncReducers))(nextState, action);
  };
}

/**
 * Utility Function to register Reducers on the fly.
 * @param name
 * @param reducer
 */
function registerLazyReducer(name, reducer) {
  if (!reduxStore.asyncReducers[name]) {
    if (process.env.NODE_ENV === 'development') console.info("Registering Lazy Reducer " + name + " ...");
    reduxStore.asyncReducers[name] = reducer;
    reduxStore.replaceReducer(combineReducersWithSugar(reduxStore.asyncReducers));
    if (process.env.NODE_ENV === 'development') console.info("Lazy Reducer " + name + " registered!");
  }
}

/**
 * The principal method of this file.
 * Starts the store.
 *
 * @param middlewares
 * @param enhancers
 */
function init(middlewares, enhancers) {
  if (middlewares === void 0) {
    middlewares = [];
  }
  if (enhancers === void 0) {
    enhancers = [];
  }
  if (!wasInit) {
    var composeEnhancers = typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || _redux.compose;

    //Connect the redux to reactotron console on dev mode
    middlewares.push(_reduxThunk.default);
    reduxStore = (0, _redux.legacy_createStore)(combineReducersWithSugar(), composeEnhancers.apply(void 0, [_redux.applyMiddleware.apply(void 0, middlewares)].concat(enhancers)));
    reduxStore.asyncReducers = {};
    wasInit = true;
    if (process.env.NODE_ENV === 'development') console.info("ReduxStore: The Store was created!", reduxStore);
    return reduxStore;
  } else {
    if (process.env.NODE_ENV === 'development') console.info("ReduxStore: WARN! The Store was already created! Returning the current instance.");
    return reduxStore;
  }
}

/**
 * Return the current value of reduxStore
 *
 * @returns {reduxStore}
 */
function getStore() {
  return reduxStore;
}
var _default = {
  init: init,
  getStore: getStore,
  registerLazyReducer: registerLazyReducer
};
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6WyJkZWZhdWx0UmVkdWNlcnMiLCJiYXNlIiwiQmFzZVJlZHVjZXIiLCJ3YXNJbml0IiwicmVkdXhTdG9yZSIsImRlZXBDbG9uZSIsImN1cnJTdGF0ZSIsIkpTT04iLCJwYXJzZSIsInN0cmluZ2lmeSIsImNvbWJpbmVSZWR1Y2Vyc1dpdGhTdWdhciIsImFzeW5jUmVkdWNlcnMiLCJzdGF0ZSIsImFjdGlvbiIsIm5leHRTdGF0ZSIsInR5cGUiLCJDTEVBUl9SRURVQ0VSX0lORk8iLCJyZWR1Y2VyIiwiQXJyYXkiLCJpc0FycmF5IiwiZGF0YVRhcmdldCIsImZvckVhY2giLCJmaWVsZCIsImhhc093blByb3BlcnR5IiwiY2xlYW5WYWx1ZSIsImUiLCJjb25zb2xlIiwiZXJyb3IiLCJPYmplY3QiLCJrZXlzIiwiY29tYmluZVJlZHVjZXJzIiwicmVnaXN0ZXJMYXp5UmVkdWNlciIsIm5hbWUiLCJwcm9jZXNzIiwiZW52IiwiTk9ERV9FTlYiLCJpbmZvIiwicmVwbGFjZVJlZHVjZXIiLCJpbml0IiwibWlkZGxld2FyZXMiLCJlbmhhbmNlcnMiLCJjb21wb3NlRW5oYW5jZXJzIiwid2luZG93IiwiX19SRURVWF9ERVZUT09MU19FWFRFTlNJT05fQ09NUE9TRV9fIiwiY29tcG9zZSIsInB1c2giLCJ0aHVuayIsImxlZ2FjeV9jcmVhdGVTdG9yZSIsImFwcGx5TWlkZGxld2FyZSIsImdldFN0b3JlIl0sInNvdXJjZXMiOlsiLi4vLi4vc3JjL3JlZHV4L1JlZHV4U3RvcmUuanMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHthcHBseU1pZGRsZXdhcmUsIGNvbWJpbmVSZWR1Y2VycywgY29tcG9zZSwgbGVnYWN5X2NyZWF0ZVN0b3JlfSBmcm9tICdyZWR1eCc7XG5pbXBvcnQgdGh1bmsgZnJvbSAncmVkdXgtdGh1bmsnO1xuaW1wb3J0IEJhc2VSZWR1Y2VyIGZyb20gXCIuL0Jhc2VSZWR1Y2VyXCI7XG5pbXBvcnQge0NMRUFSX1JFRFVDRVJfSU5GT30gZnJvbSBcIi4vQmFzZVJBVHlwZXNcIjtcblxuXG4vKipcbiAqIENyZWF0ZXMgdGhlIFJlZHV4IFN0b3JlIHdpdGggYSBkZWZhdWx0IGJhc2UgcmVkdWNlclxuICovXG5cbmNvbnN0IGRlZmF1bHRSZWR1Y2VycyA9IHtcblx0YmFzZTogQmFzZVJlZHVjZXIsXG59O1xuXG4vKipcbiAqIFJlZmVyZW5jZXMgdG8gc3RvcmVcbiAqL1xubGV0IHdhc0luaXQgPSBmYWxzZTtcbmxldCByZWR1eFN0b3JlID0gbnVsbDtcblxuXG4vKipcbiAqIERlZXAgY2xvbmVzIGEgSlNPTiBzdGF0ZVxuICogQHBhcmFtIGN1cnJTdGF0ZVxuICovXG5leHBvcnQgZnVuY3Rpb24gZGVlcENsb25lKGN1cnJTdGF0ZSkge1xuXHRyZXR1cm4gSlNPTi5wYXJzZShKU09OLnN0cmluZ2lmeShjdXJyU3RhdGUpKTtcbn1cblxuXG4vKipcbiAqIFV0aWxpdHkgZnVuY3Rpb24gdGhhdCBjb21iaW5lcyByZWR1Y2VycyBlbmFibGluZyBnbG9iYWwgY2F0Y2ggb2YgQ0xFQVJfUkVEVUNFUl9JTkZPXG4gKiBAcGFyYW0gYXN5bmNSZWR1Y2Vyc1xuICovXG5mdW5jdGlvbiBjb21iaW5lUmVkdWNlcnNXaXRoU3VnYXIoYXN5bmNSZWR1Y2VycyA9IHt9KSB7XG5cdHJldHVybiAoc3RhdGUsIGFjdGlvbikgPT4ge1xuXHRcdGxldCBuZXh0U3RhdGUgPSBzdGF0ZTtcblxuXHRcdGlmIChhY3Rpb24udHlwZSA9PT0gQ0xFQVJfUkVEVUNFUl9JTkZPKSB7XG5cdFx0XHRpZiAobmV4dFN0YXRlW2FjdGlvbi5yZWR1Y2VyXSkge1xuXG5cdFx0XHRcdGlmICghQXJyYXkuaXNBcnJheShhY3Rpb24uZGF0YVRhcmdldCkpIHtcblx0XHRcdFx0XHRhY3Rpb24uZGF0YVRhcmdldCA9IFthY3Rpb24uZGF0YVRhcmdldF07XG5cdFx0XHRcdH1cblxuXHRcdFx0XHRhY3Rpb24uZGF0YVRhcmdldC5mb3JFYWNoKGZpZWxkID0+IHtcblx0XHRcdFx0XHRpZiAobmV4dFN0YXRlW2FjdGlvbi5yZWR1Y2VyXS5oYXNPd25Qcm9wZXJ0eShmaWVsZCkpIHtcblx0XHRcdFx0XHRcdHRyeSB7XG5cdFx0XHRcdFx0XHRcdG5leHRTdGF0ZVthY3Rpb24ucmVkdWNlcl1bZmllbGRdID0gYWN0aW9uLmNsZWFuVmFsdWU7XG5cdFx0XHRcdFx0XHR9IGNhdGNoIChlKSB7XG5cdFx0XHRcdFx0XHRcdGNvbnNvbGUuZXJyb3IoYFtDTEVBUl9SRURVQ0VSX0RBVEFdIFRoZSBnaXZlbiByZWR1Y2VyICcke2FjdGlvbi5yZWR1Y2VyfScgY2FuJ3QgYWxsb3cgc3RhdGUgY2hhbmdlIGZyb20gdGhlIGdlbmVyaWMgJ0NMRUFSX1JFRFVDRVJfREFUQScgYWN0aW9uLiBJZiB5b3UgYXJlIHVzaW5nIGltbWVyIG9yIHNvbWV0aGluZyBlbHNlLCBtYXliZSB5b3Ugc2hvdWxkIGV2aWN0IHRoaXMgYXBwcm9hY2ggYW5kIGNyZWF0ZSBjdXN0b20gY2xlYW4gZGF0YSBhY3Rpb25zLmApXG5cdFx0XHRcdFx0XHRcdGNvbnNvbGUuZXJyb3IoYFtDTEVBUl9SRURVQ0VSX0RBVEFdYCwgZSlcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0Y29uc29sZS5lcnJvcihgW0NMRUFSX1JFRFVDRVJfREFUQV0gVGhlIGdpdmVuIHJlZHVjZXIgJyR7YWN0aW9uLnJlZHVjZXJ9JyBkb24ndCBoYXMgYSBkZWZpbmVkIHByb3BlcnR5ICcke2ZpZWxkfScuIEN1cnJlbnQgZXhpc3Rpbmcga2V5cyBhcmU6ICR7T2JqZWN0LmtleXMobmV4dFN0YXRlW2FjdGlvbi5yZWR1Y2VyXSl9YClcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH0pO1xuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0Y29uc29sZS5lcnJvcihgW0NMRUFSX1JFRFVDRVJfREFUQV0gVGhlIGdpdmVuIHJlZHVjZXIgJyR7YWN0aW9uLnJlZHVjZXJ9JyBkb2Vzbid0IGV4aXN0cy4gQ3VycmVudCBleGlzdGluZyByZWR1Y2VycyBhcmU6ICR7T2JqZWN0LmtleXMobmV4dFN0YXRlKX1gKVxuXHRcdFx0fVxuXHRcdH1cblxuXHRcdHJldHVybiBjb21iaW5lUmVkdWNlcnMoey4uLmRlZmF1bHRSZWR1Y2VycywgLi4uYXN5bmNSZWR1Y2Vyc30pKG5leHRTdGF0ZSwgYWN0aW9uKTtcblx0fTtcbn1cblxuXG4vKipcbiAqIFV0aWxpdHkgRnVuY3Rpb24gdG8gcmVnaXN0ZXIgUmVkdWNlcnMgb24gdGhlIGZseS5cbiAqIEBwYXJhbSBuYW1lXG4gKiBAcGFyYW0gcmVkdWNlclxuICovXG5mdW5jdGlvbiByZWdpc3RlckxhenlSZWR1Y2VyKG5hbWUsIHJlZHVjZXIpIHtcblx0aWYgKCFyZWR1eFN0b3JlLmFzeW5jUmVkdWNlcnNbbmFtZV0pIHtcblx0XHRpZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgPT09ICdkZXZlbG9wbWVudCcpXG5cdFx0XHRjb25zb2xlLmluZm8oYFJlZ2lzdGVyaW5nIExhenkgUmVkdWNlciAke25hbWV9IC4uLmApO1xuXG5cdFx0cmVkdXhTdG9yZS5hc3luY1JlZHVjZXJzW25hbWVdID0gcmVkdWNlcjtcblx0XHRyZWR1eFN0b3JlLnJlcGxhY2VSZWR1Y2VyKGNvbWJpbmVSZWR1Y2Vyc1dpdGhTdWdhcihyZWR1eFN0b3JlLmFzeW5jUmVkdWNlcnMpKTtcblxuXHRcdGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViA9PT0gJ2RldmVsb3BtZW50Jylcblx0XHRcdGNvbnNvbGUuaW5mbyhgTGF6eSBSZWR1Y2VyICR7bmFtZX0gcmVnaXN0ZXJlZCFgKTtcblx0fVxufVxuXG5cbi8qKlxuICogVGhlIHByaW5jaXBhbCBtZXRob2Qgb2YgdGhpcyBmaWxlLlxuICogU3RhcnRzIHRoZSBzdG9yZS5cbiAqXG4gKiBAcGFyYW0gbWlkZGxld2FyZXNcbiAqIEBwYXJhbSBlbmhhbmNlcnNcbiAqL1xuZnVuY3Rpb24gaW5pdChtaWRkbGV3YXJlcyA9IFtdLCBlbmhhbmNlcnMgPSBbXSkge1xuXHRpZiAoIXdhc0luaXQpIHtcblx0XHRjb25zdCBjb21wb3NlRW5oYW5jZXJzID0gKHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnICYmIHdpbmRvdy5fX1JFRFVYX0RFVlRPT0xTX0VYVEVOU0lPTl9DT01QT1NFX18pIHx8IGNvbXBvc2U7XG5cblx0XHQvL0Nvbm5lY3QgdGhlIHJlZHV4IHRvIHJlYWN0b3Ryb24gY29uc29sZSBvbiBkZXYgbW9kZVxuXHRcdG1pZGRsZXdhcmVzLnB1c2godGh1bmspO1xuXHRcdHJlZHV4U3RvcmUgPSBsZWdhY3lfY3JlYXRlU3RvcmUoY29tYmluZVJlZHVjZXJzV2l0aFN1Z2FyKCksXG5cdFx0XHRjb21wb3NlRW5oYW5jZXJzKGFwcGx5TWlkZGxld2FyZSguLi5taWRkbGV3YXJlcyksIC4uLmVuaGFuY2Vycylcblx0XHQpO1xuXG5cdFx0cmVkdXhTdG9yZS5hc3luY1JlZHVjZXJzID0ge307XG5cdFx0d2FzSW5pdCA9IHRydWU7XG5cblx0XHRpZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgPT09ICdkZXZlbG9wbWVudCcpXG5cdFx0XHRjb25zb2xlLmluZm8oXCJSZWR1eFN0b3JlOiBUaGUgU3RvcmUgd2FzIGNyZWF0ZWQhXCIsIHJlZHV4U3RvcmUpXG5cblx0XHRyZXR1cm4gcmVkdXhTdG9yZTtcblx0fSBlbHNlIHtcblx0XHRpZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgPT09ICdkZXZlbG9wbWVudCcpXG5cdFx0XHRjb25zb2xlLmluZm8oXCJSZWR1eFN0b3JlOiBXQVJOISBUaGUgU3RvcmUgd2FzIGFscmVhZHkgY3JlYXRlZCEgUmV0dXJuaW5nIHRoZSBjdXJyZW50IGluc3RhbmNlLlwiKVxuXG5cdFx0cmV0dXJuIHJlZHV4U3RvcmU7XG5cdH1cbn1cblxuLyoqXG4gKiBSZXR1cm4gdGhlIGN1cnJlbnQgdmFsdWUgb2YgcmVkdXhTdG9yZVxuICpcbiAqIEByZXR1cm5zIHtyZWR1eFN0b3JlfVxuICovXG5mdW5jdGlvbiBnZXRTdG9yZSgpIHtcblx0cmV0dXJuIHJlZHV4U3RvcmU7XG59XG5cbmV4cG9ydCBkZWZhdWx0IHtcblx0aW5pdDogaW5pdCxcblx0Z2V0U3RvcmU6IGdldFN0b3JlLFxuXHRyZWdpc3RlckxhenlSZWR1Y2VyXG59O1xuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBaUQ7QUFBQTtBQUFBO0FBQUE7QUFHakQ7QUFDQTtBQUNBOztBQUVBLElBQU1BLGVBQWUsR0FBRztFQUN2QkMsSUFBSSxFQUFFQztBQUNQLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0EsSUFBSUMsT0FBTyxHQUFHLEtBQUs7QUFDbkIsSUFBSUMsVUFBVSxHQUFHLElBQUk7O0FBR3JCO0FBQ0E7QUFDQTtBQUNBO0FBQ08sU0FBU0MsU0FBUyxDQUFDQyxTQUFTLEVBQUU7RUFDcEMsT0FBT0MsSUFBSSxDQUFDQyxLQUFLLENBQUNELElBQUksQ0FBQ0UsU0FBUyxDQUFDSCxTQUFTLENBQUMsQ0FBQztBQUM3Qzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVNJLHdCQUF3QixDQUFDQyxhQUFhLEVBQU87RUFBQSxJQUFwQkEsYUFBYTtJQUFiQSxhQUFhLEdBQUcsQ0FBQyxDQUFDO0VBQUE7RUFDbkQsT0FBTyxVQUFDQyxLQUFLLEVBQUVDLE1BQU0sRUFBSztJQUN6QixJQUFJQyxTQUFTLEdBQUdGLEtBQUs7SUFFckIsSUFBSUMsTUFBTSxDQUFDRSxJQUFJLEtBQUtDLCtCQUFrQixFQUFFO01BQ3ZDLElBQUlGLFNBQVMsQ0FBQ0QsTUFBTSxDQUFDSSxPQUFPLENBQUMsRUFBRTtRQUU5QixJQUFJLENBQUNDLEtBQUssQ0FBQ0MsT0FBTyxDQUFDTixNQUFNLENBQUNPLFVBQVUsQ0FBQyxFQUFFO1VBQ3RDUCxNQUFNLENBQUNPLFVBQVUsR0FBRyxDQUFDUCxNQUFNLENBQUNPLFVBQVUsQ0FBQztRQUN4QztRQUVBUCxNQUFNLENBQUNPLFVBQVUsQ0FBQ0MsT0FBTyxDQUFDLFVBQUFDLEtBQUssRUFBSTtVQUNsQyxJQUFJUixTQUFTLENBQUNELE1BQU0sQ0FBQ0ksT0FBTyxDQUFDLENBQUNNLGNBQWMsQ0FBQ0QsS0FBSyxDQUFDLEVBQUU7WUFDcEQsSUFBSTtjQUNIUixTQUFTLENBQUNELE1BQU0sQ0FBQ0ksT0FBTyxDQUFDLENBQUNLLEtBQUssQ0FBQyxHQUFHVCxNQUFNLENBQUNXLFVBQVU7WUFDckQsQ0FBQyxDQUFDLE9BQU9DLENBQUMsRUFBRTtjQUNYQyxPQUFPLENBQUNDLEtBQUssOENBQTRDZCxNQUFNLENBQUNJLE9BQU8sbU1BQWdNO2NBQ3ZRUyxPQUFPLENBQUNDLEtBQUsseUJBQXlCRixDQUFDLENBQUM7WUFDekM7VUFDRCxDQUFDLE1BQU07WUFDTkMsT0FBTyxDQUFDQyxLQUFLLDhDQUE0Q2QsTUFBTSxDQUFDSSxPQUFPLHdDQUFtQ0ssS0FBSyxzQ0FBaUNNLE1BQU0sQ0FBQ0MsSUFBSSxDQUFDZixTQUFTLENBQUNELE1BQU0sQ0FBQ0ksT0FBTyxDQUFDLENBQUMsQ0FBRztVQUMxTDtRQUNELENBQUMsQ0FBQztNQUNILENBQUMsTUFBTTtRQUNOUyxPQUFPLENBQUNDLEtBQUssOENBQTRDZCxNQUFNLENBQUNJLE9BQU8seURBQW9EVyxNQUFNLENBQUNDLElBQUksQ0FBQ2YsU0FBUyxDQUFDLENBQUc7TUFDcko7SUFDRDtJQUVBLE9BQU8sSUFBQWdCLHNCQUFlLGtDQUFLOUIsZUFBZSxHQUFLVyxhQUFhLEVBQUUsQ0FBQ0csU0FBUyxFQUFFRCxNQUFNLENBQUM7RUFDbEYsQ0FBQztBQUNGOztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTa0IsbUJBQW1CLENBQUNDLElBQUksRUFBRWYsT0FBTyxFQUFFO0VBQzNDLElBQUksQ0FBQ2IsVUFBVSxDQUFDTyxhQUFhLENBQUNxQixJQUFJLENBQUMsRUFBRTtJQUNwQyxJQUFJQyxPQUFPLENBQUNDLEdBQUcsQ0FBQ0MsUUFBUSxLQUFLLGFBQWEsRUFDekNULE9BQU8sQ0FBQ1UsSUFBSSwrQkFBNkJKLElBQUksVUFBTztJQUVyRDVCLFVBQVUsQ0FBQ08sYUFBYSxDQUFDcUIsSUFBSSxDQUFDLEdBQUdmLE9BQU87SUFDeENiLFVBQVUsQ0FBQ2lDLGNBQWMsQ0FBQzNCLHdCQUF3QixDQUFDTixVQUFVLENBQUNPLGFBQWEsQ0FBQyxDQUFDO0lBRTdFLElBQUlzQixPQUFPLENBQUNDLEdBQUcsQ0FBQ0MsUUFBUSxLQUFLLGFBQWEsRUFDekNULE9BQU8sQ0FBQ1UsSUFBSSxtQkFBaUJKLElBQUksa0JBQWU7RUFDbEQ7QUFDRDs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVNNLElBQUksQ0FBQ0MsV0FBVyxFQUFPQyxTQUFTLEVBQU87RUFBQSxJQUFsQ0QsV0FBVztJQUFYQSxXQUFXLEdBQUcsRUFBRTtFQUFBO0VBQUEsSUFBRUMsU0FBUztJQUFUQSxTQUFTLEdBQUcsRUFBRTtFQUFBO0VBQzdDLElBQUksQ0FBQ3JDLE9BQU8sRUFBRTtJQUNiLElBQU1zQyxnQkFBZ0IsR0FBSSxPQUFPQyxNQUFNLEtBQUssV0FBVyxJQUFJQSxNQUFNLENBQUNDLG9DQUFvQyxJQUFLQyxjQUFPOztJQUVsSDtJQUNBTCxXQUFXLENBQUNNLElBQUksQ0FBQ0MsbUJBQUssQ0FBQztJQUN2QjFDLFVBQVUsR0FBRyxJQUFBMkMseUJBQWtCLEVBQUNyQyx3QkFBd0IsRUFBRSxFQUN6RCtCLGdCQUFnQixnQkFBQ08sc0JBQWUsZUFBSVQsV0FBVyxDQUFDLFNBQUtDLFNBQVMsRUFBQyxDQUMvRDtJQUVEcEMsVUFBVSxDQUFDTyxhQUFhLEdBQUcsQ0FBQyxDQUFDO0lBQzdCUixPQUFPLEdBQUcsSUFBSTtJQUVkLElBQUk4QixPQUFPLENBQUNDLEdBQUcsQ0FBQ0MsUUFBUSxLQUFLLGFBQWEsRUFDekNULE9BQU8sQ0FBQ1UsSUFBSSxDQUFDLG9DQUFvQyxFQUFFaEMsVUFBVSxDQUFDO0lBRS9ELE9BQU9BLFVBQVU7RUFDbEIsQ0FBQyxNQUFNO0lBQ04sSUFBSTZCLE9BQU8sQ0FBQ0MsR0FBRyxDQUFDQyxRQUFRLEtBQUssYUFBYSxFQUN6Q1QsT0FBTyxDQUFDVSxJQUFJLENBQUMsa0ZBQWtGLENBQUM7SUFFakcsT0FBT2hDLFVBQVU7RUFDbEI7QUFDRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUzZDLFFBQVEsR0FBRztFQUNuQixPQUFPN0MsVUFBVTtBQUNsQjtBQUFDLGVBRWM7RUFDZGtDLElBQUksRUFBRUEsSUFBSTtFQUNWVyxRQUFRLEVBQUVBLFFBQVE7RUFDbEJsQixtQkFBbUIsRUFBbkJBO0FBQ0QsQ0FBQztBQUFBIn0=