"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _redux = require("redux");

var _reduxThunk = _interopRequireDefault(require("redux-thunk"));

var _BaseReducer = _interopRequireDefault(require("./BaseReducer"));

var _BaseRATypes = require("./BaseRATypes");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * Creates the Redux Store with a default base reducer
 */
var defaultReducers = {
  base: _BaseReducer.default
};
/**
 * References to store
 */

var wasInit = false;
var reduxStore = null;
/**
 * Utility function that combine reducers enabling global catch of CLEAR_REDUCER_INFO
 * @param asyncReducers
 * @returns {function(*=, *=): *}
 */

var combineReducersWithSugar = function combineReducersWithSugar(asyncReducers) {
  if (asyncReducers === void 0) {
    asyncReducers = {};
  }

  return function (state, action) {
    if (action.type === _BaseRATypes.CLEAR_REDUCER_INFO) {
      if (state[action.reducer]) {
        state = _objectSpread({}, state);
        if (!Array.isArray(action.dataTarget)) action.dataTarget = [action.dataTarget];
        action.dataTarget.forEach(function (field) {
          state[action.reducer][field] = action.cleanValue;
        });
      } else {
        console.error("[CLEAR_REDUCER_DATA] The given reducer '" + action.reducer + "' doesn't exists. Current existing reducers are: " + Object.keys(state));
      }
    }

    return (0, _redux.combineReducers)(_objectSpread(_objectSpread({}, defaultReducers), asyncReducers))(state, action);
  };
};
/**
 * Utility Function to register Reducers on the fly.
 * @param name
 * @param reducer
 */


var registerLazyReducer = function registerLazyReducer(name, reducer) {
  if (!reduxStore.asyncReducers[name]) {
    if (process.env.NODE_ENV === 'development') console.info("Registering Lazy Reducer " + name + " ...");
    reduxStore.asyncReducers[name] = reducer;
    reduxStore.replaceReducer(combineReducersWithSugar(reduxStore.asyncReducers));
    if (process.env.NODE_ENV === 'development') console.info("Lazy Reducer " + name + " registered!");
  }
};
/**
 * The principal method of this file.
 * Starts the store.
 *
 * @param middlewares
 * @param enhancers
 * @returns {Store}
 */


var init = function init(middlewares, enhancers) {
  if (middlewares === void 0) {
    middlewares = [];
  }

  if (enhancers === void 0) {
    enhancers = [];
  }

  if (!wasInit) {
    var composeEnhancers = typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || _redux.compose; //Connect the redux to reactotron console on dev mode

    middlewares.push(_reduxThunk.default);
    reduxStore = (0, _redux.createStore)(combineReducersWithSugar(), composeEnhancers.apply(void 0, [_redux.applyMiddleware.apply(void 0, middlewares)].concat(enhancers)));
    reduxStore.asyncReducers = {};
    wasInit = true;
    if (process.env.NODE_ENV === 'development') console.info("ReduxStore: The Store was created!", reduxStore);
    return reduxStore;
  } else {
    if (process.env.NODE_ENV === 'development') console.info("ReduxStore: WARN! The Store was already created! Returning the current instance.");
    return reduxStore;
  }
};

var getStore = function getStore() {
  return reduxStore;
};

var _default = {
  init: init,
  getStore: getStore,
  registerLazyReducer: registerLazyReducer
};
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9yZWR1eC9SZWR1eFN0b3JlLmpzIl0sIm5hbWVzIjpbImRlZmF1bHRSZWR1Y2VycyIsImJhc2UiLCJCYXNlUmVkdWNlciIsIndhc0luaXQiLCJyZWR1eFN0b3JlIiwiY29tYmluZVJlZHVjZXJzV2l0aFN1Z2FyIiwiYXN5bmNSZWR1Y2VycyIsInN0YXRlIiwiYWN0aW9uIiwidHlwZSIsIkNMRUFSX1JFRFVDRVJfSU5GTyIsInJlZHVjZXIiLCJBcnJheSIsImlzQXJyYXkiLCJkYXRhVGFyZ2V0IiwiZm9yRWFjaCIsImZpZWxkIiwiY2xlYW5WYWx1ZSIsImNvbnNvbGUiLCJlcnJvciIsIk9iamVjdCIsImtleXMiLCJyZWdpc3RlckxhenlSZWR1Y2VyIiwibmFtZSIsInByb2Nlc3MiLCJlbnYiLCJOT0RFX0VOViIsImluZm8iLCJyZXBsYWNlUmVkdWNlciIsImluaXQiLCJtaWRkbGV3YXJlcyIsImVuaGFuY2VycyIsImNvbXBvc2VFbmhhbmNlcnMiLCJ3aW5kb3ciLCJfX1JFRFVYX0RFVlRPT0xTX0VYVEVOU0lPTl9DT01QT1NFX18iLCJjb21wb3NlIiwicHVzaCIsInRodW5rIiwiYXBwbHlNaWRkbGV3YXJlIiwiZ2V0U3RvcmUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7QUFDQTs7QUFDQTs7QUFDQTs7Ozs7Ozs7OztBQUVBO0FBQ0E7QUFDQTtBQUVBLElBQU1BLGVBQWUsR0FBRztBQUN2QkMsRUFBQUEsSUFBSSxFQUFFQztBQURpQixDQUF4QjtBQUlBO0FBQ0E7QUFDQTs7QUFDQSxJQUFJQyxPQUFPLEdBQUcsS0FBZDtBQUNBLElBQUlDLFVBQVUsR0FBRyxJQUFqQjtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0EsSUFBTUMsd0JBQXdCLEdBQUcsU0FBM0JBLHdCQUEyQixDQUFDQyxhQUFELEVBQXdCO0FBQUEsTUFBdkJBLGFBQXVCO0FBQXZCQSxJQUFBQSxhQUF1QixHQUFQLEVBQU87QUFBQTs7QUFDeEQsU0FBTyxVQUFDQyxLQUFELEVBQVFDLE1BQVIsRUFBbUI7QUFDekIsUUFBSUEsTUFBTSxDQUFDQyxJQUFQLEtBQWdCQywrQkFBcEIsRUFBd0M7QUFDdkMsVUFBSUgsS0FBSyxDQUFDQyxNQUFNLENBQUNHLE9BQVIsQ0FBVCxFQUEyQjtBQUMxQkosUUFBQUEsS0FBSyxxQkFBT0EsS0FBUCxDQUFMO0FBRUEsWUFBSSxDQUFDSyxLQUFLLENBQUNDLE9BQU4sQ0FBY0wsTUFBTSxDQUFDTSxVQUFyQixDQUFMLEVBQ0NOLE1BQU0sQ0FBQ00sVUFBUCxHQUFvQixDQUFDTixNQUFNLENBQUNNLFVBQVIsQ0FBcEI7QUFFRE4sUUFBQUEsTUFBTSxDQUFDTSxVQUFQLENBQWtCQyxPQUFsQixDQUEwQixVQUFBQyxLQUFLLEVBQUk7QUFDbENULFVBQUFBLEtBQUssQ0FBQ0MsTUFBTSxDQUFDRyxPQUFSLENBQUwsQ0FBc0JLLEtBQXRCLElBQStCUixNQUFNLENBQUNTLFVBQXRDO0FBQ0EsU0FGRDtBQUdBLE9BVEQsTUFTTztBQUNOQyxRQUFBQSxPQUFPLENBQUNDLEtBQVIsOENBQXlEWCxNQUFNLENBQUNHLE9BQWhFLHlEQUEySFMsTUFBTSxDQUFDQyxJQUFQLENBQVlkLEtBQVosQ0FBM0g7QUFDQTtBQUNEOztBQUVELFdBQU8sNERBQW9CUCxlQUFwQixHQUF3Q00sYUFBeEMsR0FBd0RDLEtBQXhELEVBQStEQyxNQUEvRCxDQUFQO0FBQ0EsR0FqQkQ7QUFrQkEsQ0FuQkQ7QUFzQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EsSUFBTWMsbUJBQW1CLEdBQUcsU0FBdEJBLG1CQUFzQixDQUFDQyxJQUFELEVBQU9aLE9BQVAsRUFBbUI7QUFDOUMsTUFBSSxDQUFDUCxVQUFVLENBQUNFLGFBQVgsQ0FBeUJpQixJQUF6QixDQUFMLEVBQXFDO0FBQ3BDLFFBQUlDLE9BQU8sQ0FBQ0MsR0FBUixDQUFZQyxRQUFaLEtBQXlCLGFBQTdCLEVBQ0NSLE9BQU8sQ0FBQ1MsSUFBUiwrQkFBeUNKLElBQXpDO0FBRURuQixJQUFBQSxVQUFVLENBQUNFLGFBQVgsQ0FBeUJpQixJQUF6QixJQUFpQ1osT0FBakM7QUFDQVAsSUFBQUEsVUFBVSxDQUFDd0IsY0FBWCxDQUEwQnZCLHdCQUF3QixDQUFDRCxVQUFVLENBQUNFLGFBQVosQ0FBbEQ7QUFFQSxRQUFJa0IsT0FBTyxDQUFDQyxHQUFSLENBQVlDLFFBQVosS0FBeUIsYUFBN0IsRUFDQ1IsT0FBTyxDQUFDUyxJQUFSLG1CQUE2QkosSUFBN0I7QUFDRDtBQUNELENBWEQ7QUFjQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQSxJQUFNTSxJQUFJLEdBQUcsU0FBUEEsSUFBTyxDQUFDQyxXQUFELEVBQW1CQyxTQUFuQixFQUFzQztBQUFBLE1BQXJDRCxXQUFxQztBQUFyQ0EsSUFBQUEsV0FBcUMsR0FBdkIsRUFBdUI7QUFBQTs7QUFBQSxNQUFuQkMsU0FBbUI7QUFBbkJBLElBQUFBLFNBQW1CLEdBQVAsRUFBTztBQUFBOztBQUNsRCxNQUFJLENBQUM1QixPQUFMLEVBQWM7QUFDYixRQUFNNkIsZ0JBQWdCLEdBQUksT0FBT0MsTUFBUCxLQUFrQixXQUFsQixJQUFpQ0EsTUFBTSxDQUFDQyxvQ0FBekMsSUFBa0ZDLGNBQTNHLENBRGEsQ0FHYjs7QUFDQUwsSUFBQUEsV0FBVyxDQUFDTSxJQUFaLENBQWlCQyxtQkFBakI7QUFDQWpDLElBQUFBLFVBQVUsR0FBRyx3QkFBWUMsd0JBQXdCLEVBQXBDLEVBQ0gyQixnQkFBZ0IsTUFBaEIsVUFBaUJNLHFDQUFtQlIsV0FBbkIsQ0FBakIsU0FBcURDLFNBQXJELEVBREcsQ0FBYjtBQUlBM0IsSUFBQUEsVUFBVSxDQUFDRSxhQUFYLEdBQTJCLEVBQTNCO0FBQ0FILElBQUFBLE9BQU8sR0FBRyxJQUFWO0FBRUEsUUFBSXFCLE9BQU8sQ0FBQ0MsR0FBUixDQUFZQyxRQUFaLEtBQXlCLGFBQTdCLEVBQ0NSLE9BQU8sQ0FBQ1MsSUFBUixDQUFhLG9DQUFiLEVBQW1EdkIsVUFBbkQ7QUFFRCxXQUFPQSxVQUFQO0FBQ0EsR0FoQkQsTUFnQk87QUFDTixRQUFJb0IsT0FBTyxDQUFDQyxHQUFSLENBQVlDLFFBQVosS0FBeUIsYUFBN0IsRUFDQ1IsT0FBTyxDQUFDUyxJQUFSLENBQWEsa0ZBQWI7QUFFRCxXQUFPdkIsVUFBUDtBQUNBO0FBQ0QsQ0F2QkQ7O0FBeUJBLElBQU1tQyxRQUFRLEdBQUcsU0FBWEEsUUFBVyxHQUFNO0FBQ3RCLFNBQU9uQyxVQUFQO0FBQ0EsQ0FGRDs7ZUFJZTtBQUNkeUIsRUFBQUEsSUFBSSxFQUFFQSxJQURRO0FBRWRVLEVBQUFBLFFBQVEsRUFBRUEsUUFGSTtBQUdkakIsRUFBQUEsbUJBQW1CLEVBQW5CQTtBQUhjLEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge2FwcGx5TWlkZGxld2FyZSwgY29tYmluZVJlZHVjZXJzLCBjb21wb3NlLCBjcmVhdGVTdG9yZX0gZnJvbSAncmVkdXgnO1xuaW1wb3J0IHRodW5rIGZyb20gJ3JlZHV4LXRodW5rJztcbmltcG9ydCBCYXNlUmVkdWNlciBmcm9tIFwiLi9CYXNlUmVkdWNlclwiO1xuaW1wb3J0IHtDTEVBUl9SRURVQ0VSX0lORk99IGZyb20gXCIuL0Jhc2VSQVR5cGVzXCI7XG5cbi8qKlxuICogQ3JlYXRlcyB0aGUgUmVkdXggU3RvcmUgd2l0aCBhIGRlZmF1bHQgYmFzZSByZWR1Y2VyXG4gKi9cblxuY29uc3QgZGVmYXVsdFJlZHVjZXJzID0ge1xuXHRiYXNlOiBCYXNlUmVkdWNlcixcbn07XG5cbi8qKlxuICogUmVmZXJlbmNlcyB0byBzdG9yZVxuICovXG5sZXQgd2FzSW5pdCA9IGZhbHNlO1xubGV0IHJlZHV4U3RvcmUgPSBudWxsO1xuXG5cbi8qKlxuICogVXRpbGl0eSBmdW5jdGlvbiB0aGF0IGNvbWJpbmUgcmVkdWNlcnMgZW5hYmxpbmcgZ2xvYmFsIGNhdGNoIG9mIENMRUFSX1JFRFVDRVJfSU5GT1xuICogQHBhcmFtIGFzeW5jUmVkdWNlcnNcbiAqIEByZXR1cm5zIHtmdW5jdGlvbigqPSwgKj0pOiAqfVxuICovXG5jb25zdCBjb21iaW5lUmVkdWNlcnNXaXRoU3VnYXIgPSAoYXN5bmNSZWR1Y2VycyA9IHt9KSA9PiB7XG5cdHJldHVybiAoc3RhdGUsIGFjdGlvbikgPT4ge1xuXHRcdGlmIChhY3Rpb24udHlwZSA9PT0gQ0xFQVJfUkVEVUNFUl9JTkZPKSB7XG5cdFx0XHRpZiAoc3RhdGVbYWN0aW9uLnJlZHVjZXJdKSB7XG5cdFx0XHRcdHN0YXRlID0gey4uLnN0YXRlfTtcblxuXHRcdFx0XHRpZiAoIUFycmF5LmlzQXJyYXkoYWN0aW9uLmRhdGFUYXJnZXQpKVxuXHRcdFx0XHRcdGFjdGlvbi5kYXRhVGFyZ2V0ID0gW2FjdGlvbi5kYXRhVGFyZ2V0XTtcblxuXHRcdFx0XHRhY3Rpb24uZGF0YVRhcmdldC5mb3JFYWNoKGZpZWxkID0+IHtcblx0XHRcdFx0XHRzdGF0ZVthY3Rpb24ucmVkdWNlcl1bZmllbGRdID0gYWN0aW9uLmNsZWFuVmFsdWU7XG5cdFx0XHRcdH0pO1xuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0Y29uc29sZS5lcnJvcihgW0NMRUFSX1JFRFVDRVJfREFUQV0gVGhlIGdpdmVuIHJlZHVjZXIgJyR7YWN0aW9uLnJlZHVjZXJ9JyBkb2Vzbid0IGV4aXN0cy4gQ3VycmVudCBleGlzdGluZyByZWR1Y2VycyBhcmU6ICR7T2JqZWN0LmtleXMoc3RhdGUpfWApXG5cdFx0XHR9XG5cdFx0fVxuXG5cdFx0cmV0dXJuIGNvbWJpbmVSZWR1Y2Vycyh7Li4uZGVmYXVsdFJlZHVjZXJzLCAuLi5hc3luY1JlZHVjZXJzfSkoc3RhdGUsIGFjdGlvbik7XG5cdH07XG59O1xuXG5cbi8qKlxuICogVXRpbGl0eSBGdW5jdGlvbiB0byByZWdpc3RlciBSZWR1Y2VycyBvbiB0aGUgZmx5LlxuICogQHBhcmFtIG5hbWVcbiAqIEBwYXJhbSByZWR1Y2VyXG4gKi9cbmNvbnN0IHJlZ2lzdGVyTGF6eVJlZHVjZXIgPSAobmFtZSwgcmVkdWNlcikgPT4ge1xuXHRpZiAoIXJlZHV4U3RvcmUuYXN5bmNSZWR1Y2Vyc1tuYW1lXSkge1xuXHRcdGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViA9PT0gJ2RldmVsb3BtZW50Jylcblx0XHRcdGNvbnNvbGUuaW5mbyhgUmVnaXN0ZXJpbmcgTGF6eSBSZWR1Y2VyICR7bmFtZX0gLi4uYCk7XG5cblx0XHRyZWR1eFN0b3JlLmFzeW5jUmVkdWNlcnNbbmFtZV0gPSByZWR1Y2VyO1xuXHRcdHJlZHV4U3RvcmUucmVwbGFjZVJlZHVjZXIoY29tYmluZVJlZHVjZXJzV2l0aFN1Z2FyKHJlZHV4U3RvcmUuYXN5bmNSZWR1Y2VycykpO1xuXG5cdFx0aWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WID09PSAnZGV2ZWxvcG1lbnQnKVxuXHRcdFx0Y29uc29sZS5pbmZvKGBMYXp5IFJlZHVjZXIgJHtuYW1lfSByZWdpc3RlcmVkIWApO1xuXHR9XG59O1xuXG5cbi8qKlxuICogVGhlIHByaW5jaXBhbCBtZXRob2Qgb2YgdGhpcyBmaWxlLlxuICogU3RhcnRzIHRoZSBzdG9yZS5cbiAqXG4gKiBAcGFyYW0gbWlkZGxld2FyZXNcbiAqIEBwYXJhbSBlbmhhbmNlcnNcbiAqIEByZXR1cm5zIHtTdG9yZX1cbiAqL1xuY29uc3QgaW5pdCA9IChtaWRkbGV3YXJlcyA9IFtdLCBlbmhhbmNlcnMgPSBbXSkgPT4ge1xuXHRpZiAoIXdhc0luaXQpIHtcblx0XHRjb25zdCBjb21wb3NlRW5oYW5jZXJzID0gKHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnICYmIHdpbmRvdy5fX1JFRFVYX0RFVlRPT0xTX0VYVEVOU0lPTl9DT01QT1NFX18pIHx8IGNvbXBvc2U7XG5cblx0XHQvL0Nvbm5lY3QgdGhlIHJlZHV4IHRvIHJlYWN0b3Ryb24gY29uc29sZSBvbiBkZXYgbW9kZVxuXHRcdG1pZGRsZXdhcmVzLnB1c2godGh1bmspO1xuXHRcdHJlZHV4U3RvcmUgPSBjcmVhdGVTdG9yZShjb21iaW5lUmVkdWNlcnNXaXRoU3VnYXIoKSxcbiAgICAgICAgICAgIGNvbXBvc2VFbmhhbmNlcnMoYXBwbHlNaWRkbGV3YXJlKC4uLm1pZGRsZXdhcmVzKSwgLi4uZW5oYW5jZXJzKVxuXHRcdCk7XG5cblx0XHRyZWR1eFN0b3JlLmFzeW5jUmVkdWNlcnMgPSB7fTtcblx0XHR3YXNJbml0ID0gdHJ1ZTtcblxuXHRcdGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViA9PT0gJ2RldmVsb3BtZW50Jylcblx0XHRcdGNvbnNvbGUuaW5mbyhcIlJlZHV4U3RvcmU6IFRoZSBTdG9yZSB3YXMgY3JlYXRlZCFcIiwgcmVkdXhTdG9yZSlcblxuXHRcdHJldHVybiByZWR1eFN0b3JlO1xuXHR9IGVsc2Uge1xuXHRcdGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViA9PT0gJ2RldmVsb3BtZW50Jylcblx0XHRcdGNvbnNvbGUuaW5mbyhcIlJlZHV4U3RvcmU6IFdBUk4hIFRoZSBTdG9yZSB3YXMgYWxyZWFkeSBjcmVhdGVkISBSZXR1cm5pbmcgdGhlIGN1cnJlbnQgaW5zdGFuY2UuXCIpXG5cblx0XHRyZXR1cm4gcmVkdXhTdG9yZTtcblx0fVxufTtcblxuY29uc3QgZ2V0U3RvcmUgPSAoKSA9PiB7XG5cdHJldHVybiByZWR1eFN0b3JlO1xufVxuXG5leHBvcnQgZGVmYXVsdCB7XG5cdGluaXQ6IGluaXQsXG5cdGdldFN0b3JlOiBnZXRTdG9yZSxcblx0cmVnaXN0ZXJMYXp5UmVkdWNlclxufTtcbiJdfQ==