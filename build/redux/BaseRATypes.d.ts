export const ASYNC_START: "ASYNC_START";
export const ASYNC_END: "ASYNC_END";
export const CLEAR_REDUCER_INFO: "CLEAR_REDUCER_INFO";
declare namespace _default {
    export { ASYNC_END };
    export { ASYNC_START };
    export { CLEAR_REDUCER_INFO };
}
export default _default;
