"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.asyncStart = void 0;

var _BaseRATypes = require("./BaseRATypes");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * Action creator that start a Async Request Action.
 * @param subtype
 * @param httpRequest
 * @returns {{subtype: string, httpRequest: null, type: string}}
 */
var asyncStart = function asyncStart(subtype, httpRequest) {
  if (subtype === void 0) {
    subtype = "EMPTY";
  }

  if (httpRequest === void 0) {
    httpRequest = null;
  }

  return {
    type: _BaseRATypes.ASYNC_START,
    subtype: subtype,
    httpRequest: httpRequest
  };
};
/**
 * Action creator that start a Async Request Action.
 * @param subtype
 * @param httpRequest
 * @returns {{subtype: string, type: string}}
 */


exports.asyncStart = asyncStart;

var asyncEnd = function asyncEnd(subtype) {
  if (subtype === void 0) {
    subtype = "EMPTY";
  }

  return {
    type: _BaseRATypes.ASYNC_END,
    subtype: subtype
  };
};
/**
 * Fires a CLEAR action to a specific reducer and reducer value.
 * @param reducer - the reducer name
 * @param dataTarget - field name on reducer, can be a array of field names
 * @param cleanValue - OPTIONAL - value to use on 'cleaning' process. Ex. undefined, 0, "", etc. Defaults is undefined
 */


var clearReducerData = function clearReducerData(reducer, dataTarget, cleanValue) {
  if (cleanValue === void 0) {
    cleanValue = undefined;
  }

  return {
    type: _BaseRATypes.CLEAR_REDUCER_INFO,
    reducer: reducer,
    dataTarget: dataTarget,
    cleanValue: cleanValue
  };
};
/**
 * Base Async Thunk for common use of async redux actions! ;D
 * @param restApiRequest - The promise returned by the Rest API Path Call
 * @param actionType - Then Redux Action Type to hit Reducer
 * @param customResponseTransform - Transform Response body results
 * @returns Function Thunk
 * rodrix
 */


var baseAsyncThunk = function baseAsyncThunk(restApiRequest, actionType, customResponseTransform) {
  if (customResponseTransform === void 0) {
    customResponseTransform = function customResponseTransform(res) {
      return res.body;
    };
  }

  return function (dispatch) {
    dispatch(asyncStart(actionType, restApiRequest));
    var hasError = false;
    restApiRequest.then(function (res) {
      hasError = res && !res.body && !res.status.toString().startsWith("20");

      if (!hasError) {
        var body = res.body ? res.body : null;
        dispatch({
          type: actionType,
          result: customResponseTransform(res, body, hasError)
        });
      }

      return res;
    }).catch(function (resErr) {
      hasError = true;
      var body = resErr.response && resErr.response.body ? resErr.response.body : null;
      dispatch({
        type: actionType,
        result: _objectSpread({
          hasError: hasError,
          status: resErr.response ? resErr.response.statusCode : 0
        }, customResponseTransform(resErr, body, hasError))
      });

      if (resErr) {
        if (resErr.response && resErr.response.req) {
          console.debug("baseAsyncThunk XHR: ERROR OCCURRED CALLING >>>> " + resErr.response.req.url, {
            request: {
              method: resErr.response.req.method,
              url: resErr.response.req.url,
              data: resErr.response.req.data,
              headers: JSON.parse(JSON.stringify(resErr.response.req)).headers
            },
            response: {
              body: resErr.response.body,
              headers: resErr.response.headers,
              code: resErr.response.statusCode
            }
          });
        } else {
          console.debug(resErr, JSON.stringify(resErr));
        }
      }

      return resErr;
    }).finally(function () {
      dispatch(asyncEnd(actionType));
    });
  };
};

var _default = {
  asyncStart: asyncStart,
  asyncEnd: asyncEnd,
  baseAsyncThunk: baseAsyncThunk,
  clearReducerData: clearReducerData
};
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9yZWR1eC9CYXNlQWN0aW9ucy5qcyJdLCJuYW1lcyI6WyJhc3luY1N0YXJ0Iiwic3VidHlwZSIsImh0dHBSZXF1ZXN0IiwidHlwZSIsIkFTWU5DX1NUQVJUIiwiYXN5bmNFbmQiLCJBU1lOQ19FTkQiLCJjbGVhclJlZHVjZXJEYXRhIiwicmVkdWNlciIsImRhdGFUYXJnZXQiLCJjbGVhblZhbHVlIiwidW5kZWZpbmVkIiwiQ0xFQVJfUkVEVUNFUl9JTkZPIiwiYmFzZUFzeW5jVGh1bmsiLCJyZXN0QXBpUmVxdWVzdCIsImFjdGlvblR5cGUiLCJjdXN0b21SZXNwb25zZVRyYW5zZm9ybSIsInJlcyIsImJvZHkiLCJkaXNwYXRjaCIsImhhc0Vycm9yIiwidGhlbiIsInN0YXR1cyIsInRvU3RyaW5nIiwic3RhcnRzV2l0aCIsInJlc3VsdCIsImNhdGNoIiwicmVzRXJyIiwicmVzcG9uc2UiLCJzdGF0dXNDb2RlIiwicmVxIiwiY29uc29sZSIsImRlYnVnIiwidXJsIiwicmVxdWVzdCIsIm1ldGhvZCIsImRhdGEiLCJoZWFkZXJzIiwiSlNPTiIsInBhcnNlIiwic3RyaW5naWZ5IiwiY29kZSIsImZpbmFsbHkiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7Ozs7Ozs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDTyxJQUFNQSxVQUFVLEdBQUcsU0FBYkEsVUFBYSxDQUFDQyxPQUFELEVBQW9CQyxXQUFwQjtBQUFBLE1BQUNELE9BQUQ7QUFBQ0EsSUFBQUEsT0FBRCxHQUFXLE9BQVg7QUFBQTs7QUFBQSxNQUFvQkMsV0FBcEI7QUFBb0JBLElBQUFBLFdBQXBCLEdBQWtDLElBQWxDO0FBQUE7O0FBQUEsU0FBNEM7QUFDckVDLElBQUFBLElBQUksRUFBRUMsd0JBRCtEO0FBRXJFSCxJQUFBQSxPQUFPLEVBQVBBLE9BRnFFO0FBR3JFQyxJQUFBQSxXQUFXLEVBQVhBO0FBSHFFLEdBQTVDO0FBQUEsQ0FBbkI7QUFPUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBQ0EsSUFBTUcsUUFBUSxHQUFHLFNBQVhBLFFBQVcsQ0FBQ0osT0FBRDtBQUFBLE1BQUNBLE9BQUQ7QUFBQ0EsSUFBQUEsT0FBRCxHQUFXLE9BQVg7QUFBQTs7QUFBQSxTQUF3QjtBQUN4Q0UsSUFBQUEsSUFBSSxFQUFFRyxzQkFEa0M7QUFFeENMLElBQUFBLE9BQU8sRUFBUEE7QUFGd0MsR0FBeEI7QUFBQSxDQUFqQjtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EsSUFBTU0sZ0JBQWdCLEdBQUcsU0FBbkJBLGdCQUFtQixDQUFDQyxPQUFELEVBQVVDLFVBQVYsRUFBc0JDLFVBQXRCO0FBQUEsTUFBc0JBLFVBQXRCO0FBQXNCQSxJQUFBQSxVQUF0QixHQUFtQ0MsU0FBbkM7QUFBQTs7QUFBQSxTQUFrRDtBQUMxRVIsSUFBQUEsSUFBSSxFQUFFUywrQkFEb0U7QUFFMUVKLElBQUFBLE9BQU8sRUFBUEEsT0FGMEU7QUFHMUVDLElBQUFBLFVBQVUsRUFBVkEsVUFIMEU7QUFJMUVDLElBQUFBLFVBQVUsRUFBVkE7QUFKMEUsR0FBbEQ7QUFBQSxDQUF6QjtBQVFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBLElBQU1HLGNBQWMsR0FBRyxTQUFqQkEsY0FBaUIsQ0FBQ0MsY0FBRCxFQUFpQkMsVUFBakIsRUFBNkJDLHVCQUE3QjtBQUFBLE1BQTZCQSx1QkFBN0I7QUFBNkJBLElBQUFBLHVCQUE3QixHQUF1RCxpQ0FBQUMsR0FBRztBQUFBLGFBQUlBLEdBQUcsQ0FBQ0MsSUFBUjtBQUFBLEtBQTFEO0FBQUE7O0FBQUEsU0FBMkUsVUFBQUMsUUFBUSxFQUFJO0FBQzdHQSxJQUFBQSxRQUFRLENBQUNuQixVQUFVLENBQUNlLFVBQUQsRUFBYUQsY0FBYixDQUFYLENBQVI7QUFFQSxRQUFJTSxRQUFRLEdBQUcsS0FBZjtBQUNBTixJQUFBQSxjQUFjLENBQ1pPLElBREYsQ0FDTyxVQUFBSixHQUFHLEVBQUk7QUFDWkcsTUFBQUEsUUFBUSxHQUFHSCxHQUFHLElBQUksQ0FBQ0EsR0FBRyxDQUFDQyxJQUFaLElBQW9CLENBQUNELEdBQUcsQ0FBQ0ssTUFBSixDQUFXQyxRQUFYLEdBQXNCQyxVQUF0QixDQUFpQyxJQUFqQyxDQUFoQzs7QUFFQSxVQUFJLENBQUNKLFFBQUwsRUFBZTtBQUNkLFlBQUlGLElBQUksR0FBR0QsR0FBRyxDQUFDQyxJQUFKLEdBQVdELEdBQUcsQ0FBQ0MsSUFBZixHQUFzQixJQUFqQztBQUNBQyxRQUFBQSxRQUFRLENBQUM7QUFBQ2hCLFVBQUFBLElBQUksRUFBRVksVUFBUDtBQUFtQlUsVUFBQUEsTUFBTSxFQUFFVCx1QkFBdUIsQ0FBQ0MsR0FBRCxFQUFNQyxJQUFOLEVBQVlFLFFBQVo7QUFBbEQsU0FBRCxDQUFSO0FBQ0E7O0FBRUQsYUFBT0gsR0FBUDtBQUNBLEtBVkYsRUFXRVMsS0FYRixDQVdRLFVBQUFDLE1BQU0sRUFBSTtBQUNoQlAsTUFBQUEsUUFBUSxHQUFHLElBQVg7QUFFQSxVQUFJRixJQUFJLEdBQUdTLE1BQU0sQ0FBQ0MsUUFBUCxJQUFtQkQsTUFBTSxDQUFDQyxRQUFQLENBQWdCVixJQUFuQyxHQUEwQ1MsTUFBTSxDQUFDQyxRQUFQLENBQWdCVixJQUExRCxHQUFpRSxJQUE1RTtBQUVBQyxNQUFBQSxRQUFRLENBQUM7QUFDUmhCLFFBQUFBLElBQUksRUFBRVksVUFERTtBQUVSVSxRQUFBQSxNQUFNO0FBQ0xMLFVBQUFBLFFBQVEsRUFBUkEsUUFESztBQUVMRSxVQUFBQSxNQUFNLEVBQUVLLE1BQU0sQ0FBQ0MsUUFBUCxHQUFrQkQsTUFBTSxDQUFDQyxRQUFQLENBQWdCQyxVQUFsQyxHQUErQztBQUZsRCxXQUdGYix1QkFBdUIsQ0FBQ1csTUFBRCxFQUFTVCxJQUFULEVBQWVFLFFBQWYsQ0FIckI7QUFGRSxPQUFELENBQVI7O0FBU0EsVUFBSU8sTUFBSixFQUFZO0FBQ1gsWUFBSUEsTUFBTSxDQUFDQyxRQUFQLElBQW1CRCxNQUFNLENBQUNDLFFBQVAsQ0FBZ0JFLEdBQXZDLEVBQTRDO0FBQzNDQyxVQUFBQSxPQUFPLENBQUNDLEtBQVIsc0RBQWlFTCxNQUFNLENBQUNDLFFBQVAsQ0FBZ0JFLEdBQWhCLENBQW9CRyxHQUFyRixFQUE0RjtBQUMzRkMsWUFBQUEsT0FBTyxFQUFFO0FBQ1JDLGNBQUFBLE1BQU0sRUFBRVIsTUFBTSxDQUFDQyxRQUFQLENBQWdCRSxHQUFoQixDQUFvQkssTUFEcEI7QUFFUkYsY0FBQUEsR0FBRyxFQUFFTixNQUFNLENBQUNDLFFBQVAsQ0FBZ0JFLEdBQWhCLENBQW9CRyxHQUZqQjtBQUdSRyxjQUFBQSxJQUFJLEVBQUVULE1BQU0sQ0FBQ0MsUUFBUCxDQUFnQkUsR0FBaEIsQ0FBb0JNLElBSGxCO0FBSVJDLGNBQUFBLE9BQU8sRUFBRUMsSUFBSSxDQUFDQyxLQUFMLENBQVdELElBQUksQ0FBQ0UsU0FBTCxDQUFlYixNQUFNLENBQUNDLFFBQVAsQ0FBZ0JFLEdBQS9CLENBQVgsRUFBZ0RPO0FBSmpELGFBRGtGO0FBTzNGVCxZQUFBQSxRQUFRLEVBQUU7QUFDVFYsY0FBQUEsSUFBSSxFQUFFUyxNQUFNLENBQUNDLFFBQVAsQ0FBZ0JWLElBRGI7QUFFVG1CLGNBQUFBLE9BQU8sRUFBRVYsTUFBTSxDQUFDQyxRQUFQLENBQWdCUyxPQUZoQjtBQUdUSSxjQUFBQSxJQUFJLEVBQUVkLE1BQU0sQ0FBQ0MsUUFBUCxDQUFnQkM7QUFIYjtBQVBpRixXQUE1RjtBQWFBLFNBZEQsTUFjTztBQUNORSxVQUFBQSxPQUFPLENBQUNDLEtBQVIsQ0FBY0wsTUFBZCxFQUFzQlcsSUFBSSxDQUFDRSxTQUFMLENBQWViLE1BQWYsQ0FBdEI7QUFDQTtBQUNEOztBQUVELGFBQU9BLE1BQVA7QUFDQSxLQTlDRixFQStDRWUsT0EvQ0YsQ0ErQ1UsWUFBTTtBQUNkdkIsTUFBQUEsUUFBUSxDQUFDZCxRQUFRLENBQUNVLFVBQUQsQ0FBVCxDQUFSO0FBQ0EsS0FqREY7QUFrREEsR0F0RHNCO0FBQUEsQ0FBdkI7O2VBd0RlO0FBQ2RmLEVBQUFBLFVBQVUsRUFBVkEsVUFEYztBQUVkSyxFQUFBQSxRQUFRLEVBQVJBLFFBRmM7QUFHZFEsRUFBQUEsY0FBYyxFQUFkQSxjQUhjO0FBSWROLEVBQUFBLGdCQUFnQixFQUFoQkE7QUFKYyxDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtBU1lOQ19FTkQsIEFTWU5DX1NUQVJULCBDTEVBUl9SRURVQ0VSX0lORk99IGZyb20gJy4vQmFzZVJBVHlwZXMnXG5cblxuLyoqXG4gKiBBY3Rpb24gY3JlYXRvciB0aGF0IHN0YXJ0IGEgQXN5bmMgUmVxdWVzdCBBY3Rpb24uXG4gKiBAcGFyYW0gc3VidHlwZVxuICogQHBhcmFtIGh0dHBSZXF1ZXN0XG4gKiBAcmV0dXJucyB7e3N1YnR5cGU6IHN0cmluZywgaHR0cFJlcXVlc3Q6IG51bGwsIHR5cGU6IHN0cmluZ319XG4gKi9cbmV4cG9ydCBjb25zdCBhc3luY1N0YXJ0ID0gKHN1YnR5cGUgPSBcIkVNUFRZXCIsIGh0dHBSZXF1ZXN0ID0gbnVsbCkgPT4gKHtcblx0dHlwZTogQVNZTkNfU1RBUlQsXG5cdHN1YnR5cGUsXG5cdGh0dHBSZXF1ZXN0XG59KTtcblxuXG4vKipcbiAqIEFjdGlvbiBjcmVhdG9yIHRoYXQgc3RhcnQgYSBBc3luYyBSZXF1ZXN0IEFjdGlvbi5cbiAqIEBwYXJhbSBzdWJ0eXBlXG4gKiBAcGFyYW0gaHR0cFJlcXVlc3RcbiAqIEByZXR1cm5zIHt7c3VidHlwZTogc3RyaW5nLCB0eXBlOiBzdHJpbmd9fVxuICovXG5jb25zdCBhc3luY0VuZCA9IChzdWJ0eXBlID0gXCJFTVBUWVwiKSA9PiAoe1xuXHR0eXBlOiBBU1lOQ19FTkQsXG5cdHN1YnR5cGUsXG59KTtcblxuXG4vKipcbiAqIEZpcmVzIGEgQ0xFQVIgYWN0aW9uIHRvIGEgc3BlY2lmaWMgcmVkdWNlciBhbmQgcmVkdWNlciB2YWx1ZS5cbiAqIEBwYXJhbSByZWR1Y2VyIC0gdGhlIHJlZHVjZXIgbmFtZVxuICogQHBhcmFtIGRhdGFUYXJnZXQgLSBmaWVsZCBuYW1lIG9uIHJlZHVjZXIsIGNhbiBiZSBhIGFycmF5IG9mIGZpZWxkIG5hbWVzXG4gKiBAcGFyYW0gY2xlYW5WYWx1ZSAtIE9QVElPTkFMIC0gdmFsdWUgdG8gdXNlIG9uICdjbGVhbmluZycgcHJvY2Vzcy4gRXguIHVuZGVmaW5lZCwgMCwgXCJcIiwgZXRjLiBEZWZhdWx0cyBpcyB1bmRlZmluZWRcbiAqL1xuY29uc3QgY2xlYXJSZWR1Y2VyRGF0YSA9IChyZWR1Y2VyLCBkYXRhVGFyZ2V0LCBjbGVhblZhbHVlID0gdW5kZWZpbmVkKSA9PiAoe1xuXHR0eXBlOiBDTEVBUl9SRURVQ0VSX0lORk8sXG5cdHJlZHVjZXIsXG5cdGRhdGFUYXJnZXQsXG5cdGNsZWFuVmFsdWVcbn0pO1xuXG5cbi8qKlxuICogQmFzZSBBc3luYyBUaHVuayBmb3IgY29tbW9uIHVzZSBvZiBhc3luYyByZWR1eCBhY3Rpb25zISA7RFxuICogQHBhcmFtIHJlc3RBcGlSZXF1ZXN0IC0gVGhlIHByb21pc2UgcmV0dXJuZWQgYnkgdGhlIFJlc3QgQVBJIFBhdGggQ2FsbFxuICogQHBhcmFtIGFjdGlvblR5cGUgLSBUaGVuIFJlZHV4IEFjdGlvbiBUeXBlIHRvIGhpdCBSZWR1Y2VyXG4gKiBAcGFyYW0gY3VzdG9tUmVzcG9uc2VUcmFuc2Zvcm0gLSBUcmFuc2Zvcm0gUmVzcG9uc2UgYm9keSByZXN1bHRzXG4gKiBAcmV0dXJucyBGdW5jdGlvbiBUaHVua1xuICogcm9kcml4XG4gKi9cbmNvbnN0IGJhc2VBc3luY1RodW5rID0gKHJlc3RBcGlSZXF1ZXN0LCBhY3Rpb25UeXBlLCBjdXN0b21SZXNwb25zZVRyYW5zZm9ybSA9IHJlcyA9PiByZXMuYm9keSkgPT4gZGlzcGF0Y2ggPT4ge1xuXHRkaXNwYXRjaChhc3luY1N0YXJ0KGFjdGlvblR5cGUsIHJlc3RBcGlSZXF1ZXN0KSk7XG5cblx0bGV0IGhhc0Vycm9yID0gZmFsc2U7XG5cdHJlc3RBcGlSZXF1ZXN0XG5cdFx0LnRoZW4ocmVzID0+IHtcblx0XHRcdGhhc0Vycm9yID0gcmVzICYmICFyZXMuYm9keSAmJiAhcmVzLnN0YXR1cy50b1N0cmluZygpLnN0YXJ0c1dpdGgoXCIyMFwiKTtcblxuXHRcdFx0aWYgKCFoYXNFcnJvcikge1xuXHRcdFx0XHRsZXQgYm9keSA9IHJlcy5ib2R5ID8gcmVzLmJvZHkgOiBudWxsO1xuXHRcdFx0XHRkaXNwYXRjaCh7dHlwZTogYWN0aW9uVHlwZSwgcmVzdWx0OiBjdXN0b21SZXNwb25zZVRyYW5zZm9ybShyZXMsIGJvZHksIGhhc0Vycm9yKX0pO1xuXHRcdFx0fVxuXG5cdFx0XHRyZXR1cm4gcmVzO1xuXHRcdH0pXG5cdFx0LmNhdGNoKHJlc0VyciA9PiB7XG5cdFx0XHRoYXNFcnJvciA9IHRydWU7XG5cblx0XHRcdGxldCBib2R5ID0gcmVzRXJyLnJlc3BvbnNlICYmIHJlc0Vyci5yZXNwb25zZS5ib2R5ID8gcmVzRXJyLnJlc3BvbnNlLmJvZHkgOiBudWxsO1xuXG5cdFx0XHRkaXNwYXRjaCh7XG5cdFx0XHRcdHR5cGU6IGFjdGlvblR5cGUsXG5cdFx0XHRcdHJlc3VsdDoge1xuXHRcdFx0XHRcdGhhc0Vycm9yLFxuXHRcdFx0XHRcdHN0YXR1czogcmVzRXJyLnJlc3BvbnNlID8gcmVzRXJyLnJlc3BvbnNlLnN0YXR1c0NvZGUgOiAwLFxuXHRcdFx0XHRcdC4uLmN1c3RvbVJlc3BvbnNlVHJhbnNmb3JtKHJlc0VyciwgYm9keSwgaGFzRXJyb3IpLFxuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblxuXHRcdFx0aWYgKHJlc0Vycikge1xuXHRcdFx0XHRpZiAocmVzRXJyLnJlc3BvbnNlICYmIHJlc0Vyci5yZXNwb25zZS5yZXEpIHtcblx0XHRcdFx0XHRjb25zb2xlLmRlYnVnKGBiYXNlQXN5bmNUaHVuayBYSFI6IEVSUk9SIE9DQ1VSUkVEIENBTExJTkcgPj4+PiAke3Jlc0Vyci5yZXNwb25zZS5yZXEudXJsfWAsIHtcblx0XHRcdFx0XHRcdHJlcXVlc3Q6IHtcblx0XHRcdFx0XHRcdFx0bWV0aG9kOiByZXNFcnIucmVzcG9uc2UucmVxLm1ldGhvZCxcblx0XHRcdFx0XHRcdFx0dXJsOiByZXNFcnIucmVzcG9uc2UucmVxLnVybCxcblx0XHRcdFx0XHRcdFx0ZGF0YTogcmVzRXJyLnJlc3BvbnNlLnJlcS5kYXRhLFxuXHRcdFx0XHRcdFx0XHRoZWFkZXJzOiBKU09OLnBhcnNlKEpTT04uc3RyaW5naWZ5KHJlc0Vyci5yZXNwb25zZS5yZXEpKS5oZWFkZXJzLFxuXHRcdFx0XHRcdFx0fSxcblx0XHRcdFx0XHRcdHJlc3BvbnNlOiB7XG5cdFx0XHRcdFx0XHRcdGJvZHk6IHJlc0Vyci5yZXNwb25zZS5ib2R5LFxuXHRcdFx0XHRcdFx0XHRoZWFkZXJzOiByZXNFcnIucmVzcG9uc2UuaGVhZGVycyxcblx0XHRcdFx0XHRcdFx0Y29kZTogcmVzRXJyLnJlc3BvbnNlLnN0YXR1c0NvZGUsXG5cdFx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdH0pO1xuXHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdGNvbnNvbGUuZGVidWcocmVzRXJyLCBKU09OLnN0cmluZ2lmeShyZXNFcnIpKTtcblx0XHRcdFx0fVxuXHRcdFx0fVxuXG5cdFx0XHRyZXR1cm4gcmVzRXJyO1xuXHRcdH0pXG5cdFx0LmZpbmFsbHkoKCkgPT4ge1xuXHRcdFx0ZGlzcGF0Y2goYXN5bmNFbmQoYWN0aW9uVHlwZSkpO1xuXHRcdH0pO1xufTtcblxuZXhwb3J0IGRlZmF1bHQge1xuXHRhc3luY1N0YXJ0LFxuXHRhc3luY0VuZCxcblx0YmFzZUFzeW5jVGh1bmssXG5cdGNsZWFyUmVkdWNlckRhdGFcbn1cbiJdfQ==