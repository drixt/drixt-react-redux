"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.asyncEnd = asyncEnd;
exports.asyncStart = asyncStart;
exports.baseAsyncThunk = baseAsyncThunk;
exports.clearReducerData = clearReducerData;
exports.default = void 0;
var _BaseRATypes = require("./BaseRATypes");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
/**
 * Action creator that start a Async Request Action.
 * @param subtype
 * @param httpRequest
 * @returns {{subtype: string, httpRequest: null, type: string}}
 */
function asyncStart(subtype, httpRequest) {
  if (subtype === void 0) {
    subtype = "EMPTY";
  }
  if (httpRequest === void 0) {
    httpRequest = null;
  }
  return {
    type: _BaseRATypes.ASYNC_START,
    subtype: subtype,
    httpRequest: httpRequest
  };
}

/**
 * Action creator that start a Async Request Action.
 * @param subtype
 * @returns {{subtype: string, type: string}}
 */
function asyncEnd(subtype) {
  if (subtype === void 0) {
    subtype = "EMPTY";
  }
  return {
    type: _BaseRATypes.ASYNC_END,
    subtype: subtype
  };
}

/**
 * Fires a CLEAR action to a specific reducer and reducer value.
 * @param reducer - the reducer name
 * @param dataTarget - field name on reducer, can be a array of field names
 * @param cleanValue - OPTIONAL - value to use on 'cleaning' process.
 * Ex. undefined, 0, "", etc.
 * Defaults to null
 */
function clearReducerData(reducer, dataTarget, cleanValue) {
  if (cleanValue === void 0) {
    cleanValue = null;
  }
  return {
    type: _BaseRATypes.CLEAR_REDUCER_INFO,
    reducer: reducer,
    dataTarget: dataTarget,
    cleanValue: cleanValue
  };
}

/**
 * Base Async Thunk for common use of async redux actions! ;D
 * @param restApiRequest - The promise returned by the Rest API Path Call
 * @param actionType - Then Redux Action Type to hit Reducer
 * @param customResponseTransform - A function to transform the response body results. Default: returns res.body
 * @param postActionCallback - OPTIONAL: A function to call a custom behavior after the thunk job
 * @returns Function Thunk
 */
function baseAsyncThunk(restApiRequest, actionType, customResponseTransform, postActionCallback) {
  if (customResponseTransform === void 0) {
    customResponseTransform = function customResponseTransform(res, body, hasError) {
      return res.body;
    };
  }
  if (postActionCallback === void 0) {
    postActionCallback = null;
  }
  return function (dispatch) {
    dispatch(asyncStart(actionType, restApiRequest));
    var hasError = false;
    restApiRequest.then(function (res) {
      hasError = res && !res.body && !res.status.toString().startsWith("20");
      if (!hasError) {
        var body = res.body ? res.body : null;
        dispatch({
          type: actionType,
          result: customResponseTransform(res, body, hasError)
        });
      }
      return res;
    }).catch(function (resErr) {
      hasError = true;
      var body = resErr.response && resErr.response.body ? resErr.response.body : null;
      dispatch({
        type: actionType,
        result: _objectSpread({
          hasError: hasError,
          status: resErr.response ? resErr.response.statusCode : 0
        }, customResponseTransform(resErr, body, hasError))
      });
      if (resErr) {
        if (resErr.response && resErr.response.req) {
          console.debug("baseAsyncThunk XHR: ERROR OCCURRED CALLING >>>> " + resErr.response.req.url, {
            request: {
              method: resErr.response.req.method,
              url: resErr.response.req.url,
              data: resErr.response.req.data,
              headers: JSON.parse(JSON.stringify(resErr.response.req)).headers
            },
            response: {
              body: resErr.response.body,
              headers: resErr.response.headers,
              code: resErr.response.statusCode
            }
          });
        } else {
          console.debug(resErr, JSON.stringify(resErr));
        }
      }
      return resErr;
    }).finally(function () {
      dispatch(asyncEnd(actionType));
      if (postActionCallback) {
        postActionCallback(dispatch);
      }
    });
  };
}
var _default = {
  asyncStart: asyncStart,
  asyncEnd: asyncEnd,
  baseAsyncThunk: baseAsyncThunk,
  clearReducerData: clearReducerData
};
exports.default = _default;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6WyJhc3luY1N0YXJ0Iiwic3VidHlwZSIsImh0dHBSZXF1ZXN0IiwidHlwZSIsIkFTWU5DX1NUQVJUIiwiYXN5bmNFbmQiLCJBU1lOQ19FTkQiLCJjbGVhclJlZHVjZXJEYXRhIiwicmVkdWNlciIsImRhdGFUYXJnZXQiLCJjbGVhblZhbHVlIiwiQ0xFQVJfUkVEVUNFUl9JTkZPIiwiYmFzZUFzeW5jVGh1bmsiLCJyZXN0QXBpUmVxdWVzdCIsImFjdGlvblR5cGUiLCJjdXN0b21SZXNwb25zZVRyYW5zZm9ybSIsInBvc3RBY3Rpb25DYWxsYmFjayIsInJlcyIsImJvZHkiLCJoYXNFcnJvciIsImRpc3BhdGNoIiwidGhlbiIsInN0YXR1cyIsInRvU3RyaW5nIiwic3RhcnRzV2l0aCIsInJlc3VsdCIsImNhdGNoIiwicmVzRXJyIiwicmVzcG9uc2UiLCJzdGF0dXNDb2RlIiwicmVxIiwiY29uc29sZSIsImRlYnVnIiwidXJsIiwicmVxdWVzdCIsIm1ldGhvZCIsImRhdGEiLCJoZWFkZXJzIiwiSlNPTiIsInBhcnNlIiwic3RyaW5naWZ5IiwiY29kZSIsImZpbmFsbHkiXSwic291cmNlcyI6WyIuLi8uLi9zcmMvcmVkdXgvQmFzZUFjdGlvbnMuanMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtBU1lOQ19FTkQsIEFTWU5DX1NUQVJULCBDTEVBUl9SRURVQ0VSX0lORk99IGZyb20gJy4vQmFzZVJBVHlwZXMnXG5cblxuLyoqXG4gKiBBY3Rpb24gY3JlYXRvciB0aGF0IHN0YXJ0IGEgQXN5bmMgUmVxdWVzdCBBY3Rpb24uXG4gKiBAcGFyYW0gc3VidHlwZVxuICogQHBhcmFtIGh0dHBSZXF1ZXN0XG4gKiBAcmV0dXJucyB7e3N1YnR5cGU6IHN0cmluZywgaHR0cFJlcXVlc3Q6IG51bGwsIHR5cGU6IHN0cmluZ319XG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBhc3luY1N0YXJ0KHN1YnR5cGUgPSBcIkVNUFRZXCIsIGh0dHBSZXF1ZXN0ID0gbnVsbCkge1xuXHRyZXR1cm4ge1xuXHRcdHR5cGU6IEFTWU5DX1NUQVJULFxuXHRcdHN1YnR5cGUsXG5cdFx0aHR0cFJlcXVlc3Rcblx0fTtcbn1cblxuXG4vKipcbiAqIEFjdGlvbiBjcmVhdG9yIHRoYXQgc3RhcnQgYSBBc3luYyBSZXF1ZXN0IEFjdGlvbi5cbiAqIEBwYXJhbSBzdWJ0eXBlXG4gKiBAcmV0dXJucyB7e3N1YnR5cGU6IHN0cmluZywgdHlwZTogc3RyaW5nfX1cbiAqL1xuZXhwb3J0IGZ1bmN0aW9uIGFzeW5jRW5kKHN1YnR5cGUgPSBcIkVNUFRZXCIpIHtcblx0cmV0dXJuIHtcblx0XHR0eXBlOiBBU1lOQ19FTkQsXG5cdFx0c3VidHlwZSxcblx0fTtcbn1cblxuXG4vKipcbiAqIEZpcmVzIGEgQ0xFQVIgYWN0aW9uIHRvIGEgc3BlY2lmaWMgcmVkdWNlciBhbmQgcmVkdWNlciB2YWx1ZS5cbiAqIEBwYXJhbSByZWR1Y2VyIC0gdGhlIHJlZHVjZXIgbmFtZVxuICogQHBhcmFtIGRhdGFUYXJnZXQgLSBmaWVsZCBuYW1lIG9uIHJlZHVjZXIsIGNhbiBiZSBhIGFycmF5IG9mIGZpZWxkIG5hbWVzXG4gKiBAcGFyYW0gY2xlYW5WYWx1ZSAtIE9QVElPTkFMIC0gdmFsdWUgdG8gdXNlIG9uICdjbGVhbmluZycgcHJvY2Vzcy5cbiAqIEV4LiB1bmRlZmluZWQsIDAsIFwiXCIsIGV0Yy5cbiAqIERlZmF1bHRzIHRvIG51bGxcbiAqL1xuZXhwb3J0IGZ1bmN0aW9uIGNsZWFyUmVkdWNlckRhdGEocmVkdWNlciwgZGF0YVRhcmdldCwgY2xlYW5WYWx1ZSA9IG51bGwpIHtcblx0cmV0dXJuIHtcblx0XHR0eXBlOiBDTEVBUl9SRURVQ0VSX0lORk8sXG5cdFx0cmVkdWNlcixcblx0XHRkYXRhVGFyZ2V0LFxuXHRcdGNsZWFuVmFsdWVcblx0fTtcbn1cblxuXG4vKipcbiAqIEJhc2UgQXN5bmMgVGh1bmsgZm9yIGNvbW1vbiB1c2Ugb2YgYXN5bmMgcmVkdXggYWN0aW9ucyEgO0RcbiAqIEBwYXJhbSByZXN0QXBpUmVxdWVzdCAtIFRoZSBwcm9taXNlIHJldHVybmVkIGJ5IHRoZSBSZXN0IEFQSSBQYXRoIENhbGxcbiAqIEBwYXJhbSBhY3Rpb25UeXBlIC0gVGhlbiBSZWR1eCBBY3Rpb24gVHlwZSB0byBoaXQgUmVkdWNlclxuICogQHBhcmFtIGN1c3RvbVJlc3BvbnNlVHJhbnNmb3JtIC0gQSBmdW5jdGlvbiB0byB0cmFuc2Zvcm0gdGhlIHJlc3BvbnNlIGJvZHkgcmVzdWx0cy4gRGVmYXVsdDogcmV0dXJucyByZXMuYm9keVxuICogQHBhcmFtIHBvc3RBY3Rpb25DYWxsYmFjayAtIE9QVElPTkFMOiBBIGZ1bmN0aW9uIHRvIGNhbGwgYSBjdXN0b20gYmVoYXZpb3IgYWZ0ZXIgdGhlIHRodW5rIGpvYlxuICogQHJldHVybnMgRnVuY3Rpb24gVGh1bmtcbiAqL1xuZXhwb3J0IGZ1bmN0aW9uIGJhc2VBc3luY1RodW5rKHJlc3RBcGlSZXF1ZXN0LFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0IGFjdGlvblR5cGUsXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHQgY3VzdG9tUmVzcG9uc2VUcmFuc2Zvcm0gPSAocmVzLCBib2R5LCBoYXNFcnJvcikgPT4gcmVzLmJvZHksXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0XHQgcG9zdEFjdGlvbkNhbGxiYWNrID0gbnVsbCkge1xuXHRyZXR1cm4gKGRpc3BhdGNoKSA9PiB7XG5cdFx0ZGlzcGF0Y2goYXN5bmNTdGFydChhY3Rpb25UeXBlLCByZXN0QXBpUmVxdWVzdCkpO1xuXG5cdFx0bGV0IGhhc0Vycm9yID0gZmFsc2U7XG5cdFx0cmVzdEFwaVJlcXVlc3Rcblx0XHRcdC50aGVuKHJlcyA9PiB7XG5cdFx0XHRcdGhhc0Vycm9yID0gcmVzICYmICFyZXMuYm9keSAmJiAhcmVzLnN0YXR1cy50b1N0cmluZygpLnN0YXJ0c1dpdGgoXCIyMFwiKTtcblxuXHRcdFx0XHRpZiAoIWhhc0Vycm9yKSB7XG5cdFx0XHRcdFx0bGV0IGJvZHkgPSByZXMuYm9keSA/IHJlcy5ib2R5IDogbnVsbDtcblx0XHRcdFx0XHRkaXNwYXRjaCh7dHlwZTogYWN0aW9uVHlwZSwgcmVzdWx0OiBjdXN0b21SZXNwb25zZVRyYW5zZm9ybShyZXMsIGJvZHksIGhhc0Vycm9yKX0pO1xuXHRcdFx0XHR9XG5cblx0XHRcdFx0cmV0dXJuIHJlcztcblx0XHRcdH0pXG5cdFx0XHQuY2F0Y2gocmVzRXJyID0+IHtcblx0XHRcdFx0aGFzRXJyb3IgPSB0cnVlO1xuXG5cdFx0XHRcdGxldCBib2R5ID0gcmVzRXJyLnJlc3BvbnNlICYmIHJlc0Vyci5yZXNwb25zZS5ib2R5ID8gcmVzRXJyLnJlc3BvbnNlLmJvZHkgOiBudWxsO1xuXG5cdFx0XHRcdGRpc3BhdGNoKHtcblx0XHRcdFx0XHR0eXBlOiBhY3Rpb25UeXBlLFxuXHRcdFx0XHRcdHJlc3VsdDoge1xuXHRcdFx0XHRcdFx0aGFzRXJyb3IsXG5cdFx0XHRcdFx0XHRzdGF0dXM6IHJlc0Vyci5yZXNwb25zZSA/IHJlc0Vyci5yZXNwb25zZS5zdGF0dXNDb2RlIDogMCxcblx0XHRcdFx0XHRcdC4uLmN1c3RvbVJlc3BvbnNlVHJhbnNmb3JtKHJlc0VyciwgYm9keSwgaGFzRXJyb3IpLFxuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSk7XG5cblx0XHRcdFx0aWYgKHJlc0Vycikge1xuXHRcdFx0XHRcdGlmIChyZXNFcnIucmVzcG9uc2UgJiYgcmVzRXJyLnJlc3BvbnNlLnJlcSkge1xuXHRcdFx0XHRcdFx0Y29uc29sZS5kZWJ1ZyhgYmFzZUFzeW5jVGh1bmsgWEhSOiBFUlJPUiBPQ0NVUlJFRCBDQUxMSU5HID4+Pj4gJHtyZXNFcnIucmVzcG9uc2UucmVxLnVybH1gLCB7XG5cdFx0XHRcdFx0XHRcdHJlcXVlc3Q6IHtcblx0XHRcdFx0XHRcdFx0XHRtZXRob2Q6IHJlc0Vyci5yZXNwb25zZS5yZXEubWV0aG9kLFxuXHRcdFx0XHRcdFx0XHRcdHVybDogcmVzRXJyLnJlc3BvbnNlLnJlcS51cmwsXG5cdFx0XHRcdFx0XHRcdFx0ZGF0YTogcmVzRXJyLnJlc3BvbnNlLnJlcS5kYXRhLFxuXHRcdFx0XHRcdFx0XHRcdGhlYWRlcnM6IEpTT04ucGFyc2UoSlNPTi5zdHJpbmdpZnkocmVzRXJyLnJlc3BvbnNlLnJlcSkpLmhlYWRlcnMsXG5cdFx0XHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0XHRcdHJlc3BvbnNlOiB7XG5cdFx0XHRcdFx0XHRcdFx0Ym9keTogcmVzRXJyLnJlc3BvbnNlLmJvZHksXG5cdFx0XHRcdFx0XHRcdFx0aGVhZGVyczogcmVzRXJyLnJlc3BvbnNlLmhlYWRlcnMsXG5cdFx0XHRcdFx0XHRcdFx0Y29kZTogcmVzRXJyLnJlc3BvbnNlLnN0YXR1c0NvZGUsXG5cdFx0XHRcdFx0XHRcdH0sXG5cdFx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0Y29uc29sZS5kZWJ1ZyhyZXNFcnIsIEpTT04uc3RyaW5naWZ5KHJlc0VycikpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXG5cdFx0XHRcdHJldHVybiByZXNFcnI7XG5cdFx0XHR9KVxuXHRcdFx0LmZpbmFsbHkoKCkgPT4ge1xuXHRcdFx0XHRkaXNwYXRjaChhc3luY0VuZChhY3Rpb25UeXBlKSk7XG5cblx0XHRcdFx0aWYgKHBvc3RBY3Rpb25DYWxsYmFjaykge1xuXHRcdFx0XHRcdHBvc3RBY3Rpb25DYWxsYmFjayhkaXNwYXRjaCk7XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXHR9O1xufVxuXG5leHBvcnQgZGVmYXVsdCB7XG4gIGFzeW5jU3RhcnQsXG4gIGFzeW5jRW5kLFxuICBiYXNlQXN5bmNUaHVuayxcbiAgY2xlYXJSZWR1Y2VyRGF0YVxufVxuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUE7QUFBd0U7QUFBQTtBQUFBO0FBR3hFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNPLFNBQVNBLFVBQVUsQ0FBQ0MsT0FBTyxFQUFZQyxXQUFXLEVBQVM7RUFBQSxJQUF2Q0QsT0FBTztJQUFQQSxPQUFPLEdBQUcsT0FBTztFQUFBO0VBQUEsSUFBRUMsV0FBVztJQUFYQSxXQUFXLEdBQUcsSUFBSTtFQUFBO0VBQy9ELE9BQU87SUFDTkMsSUFBSSxFQUFFQyx3QkFBVztJQUNqQkgsT0FBTyxFQUFQQSxPQUFPO0lBQ1BDLFdBQVcsRUFBWEE7RUFDRCxDQUFDO0FBQ0Y7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNPLFNBQVNHLFFBQVEsQ0FBQ0osT0FBTyxFQUFZO0VBQUEsSUFBbkJBLE9BQU87SUFBUEEsT0FBTyxHQUFHLE9BQU87RUFBQTtFQUN6QyxPQUFPO0lBQ05FLElBQUksRUFBRUcsc0JBQVM7SUFDZkwsT0FBTyxFQUFQQTtFQUNELENBQUM7QUFDRjs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ08sU0FBU00sZ0JBQWdCLENBQUNDLE9BQU8sRUFBRUMsVUFBVSxFQUFFQyxVQUFVLEVBQVM7RUFBQSxJQUFuQkEsVUFBVTtJQUFWQSxVQUFVLEdBQUcsSUFBSTtFQUFBO0VBQ3RFLE9BQU87SUFDTlAsSUFBSSxFQUFFUSwrQkFBa0I7SUFDeEJILE9BQU8sRUFBUEEsT0FBTztJQUNQQyxVQUFVLEVBQVZBLFVBQVU7SUFDVkMsVUFBVSxFQUFWQTtFQUNELENBQUM7QUFDRjs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ08sU0FBU0UsY0FBYyxDQUFDQyxjQUFjLEVBQzdCQyxVQUFVLEVBQ1ZDLHVCQUF1QixFQUN2QkMsa0JBQWtCLEVBQVM7RUFBQSxJQUQzQkQsdUJBQXVCO0lBQXZCQSx1QkFBdUIsR0FBRyxpQ0FBQ0UsR0FBRyxFQUFFQyxJQUFJLEVBQUVDLFFBQVE7TUFBQSxPQUFLRixHQUFHLENBQUNDLElBQUk7SUFBQTtFQUFBO0VBQUEsSUFDM0RGLGtCQUFrQjtJQUFsQkEsa0JBQWtCLEdBQUcsSUFBSTtFQUFBO0VBQ3hDLE9BQU8sVUFBQ0ksUUFBUSxFQUFLO0lBQ3BCQSxRQUFRLENBQUNwQixVQUFVLENBQUNjLFVBQVUsRUFBRUQsY0FBYyxDQUFDLENBQUM7SUFFaEQsSUFBSU0sUUFBUSxHQUFHLEtBQUs7SUFDcEJOLGNBQWMsQ0FDWlEsSUFBSSxDQUFDLFVBQUFKLEdBQUcsRUFBSTtNQUNaRSxRQUFRLEdBQUdGLEdBQUcsSUFBSSxDQUFDQSxHQUFHLENBQUNDLElBQUksSUFBSSxDQUFDRCxHQUFHLENBQUNLLE1BQU0sQ0FBQ0MsUUFBUSxFQUFFLENBQUNDLFVBQVUsQ0FBQyxJQUFJLENBQUM7TUFFdEUsSUFBSSxDQUFDTCxRQUFRLEVBQUU7UUFDZCxJQUFJRCxJQUFJLEdBQUdELEdBQUcsQ0FBQ0MsSUFBSSxHQUFHRCxHQUFHLENBQUNDLElBQUksR0FBRyxJQUFJO1FBQ3JDRSxRQUFRLENBQUM7VUFBQ2pCLElBQUksRUFBRVcsVUFBVTtVQUFFVyxNQUFNLEVBQUVWLHVCQUF1QixDQUFDRSxHQUFHLEVBQUVDLElBQUksRUFBRUMsUUFBUTtRQUFDLENBQUMsQ0FBQztNQUNuRjtNQUVBLE9BQU9GLEdBQUc7SUFDWCxDQUFDLENBQUMsQ0FDRFMsS0FBSyxDQUFDLFVBQUFDLE1BQU0sRUFBSTtNQUNoQlIsUUFBUSxHQUFHLElBQUk7TUFFZixJQUFJRCxJQUFJLEdBQUdTLE1BQU0sQ0FBQ0MsUUFBUSxJQUFJRCxNQUFNLENBQUNDLFFBQVEsQ0FBQ1YsSUFBSSxHQUFHUyxNQUFNLENBQUNDLFFBQVEsQ0FBQ1YsSUFBSSxHQUFHLElBQUk7TUFFaEZFLFFBQVEsQ0FBQztRQUNSakIsSUFBSSxFQUFFVyxVQUFVO1FBQ2hCVyxNQUFNO1VBQ0xOLFFBQVEsRUFBUkEsUUFBUTtVQUNSRyxNQUFNLEVBQUVLLE1BQU0sQ0FBQ0MsUUFBUSxHQUFHRCxNQUFNLENBQUNDLFFBQVEsQ0FBQ0MsVUFBVSxHQUFHO1FBQUMsR0FDckRkLHVCQUF1QixDQUFDWSxNQUFNLEVBQUVULElBQUksRUFBRUMsUUFBUSxDQUFDO01BRXBELENBQUMsQ0FBQztNQUVGLElBQUlRLE1BQU0sRUFBRTtRQUNYLElBQUlBLE1BQU0sQ0FBQ0MsUUFBUSxJQUFJRCxNQUFNLENBQUNDLFFBQVEsQ0FBQ0UsR0FBRyxFQUFFO1VBQzNDQyxPQUFPLENBQUNDLEtBQUssc0RBQW9ETCxNQUFNLENBQUNDLFFBQVEsQ0FBQ0UsR0FBRyxDQUFDRyxHQUFHLEVBQUk7WUFDM0ZDLE9BQU8sRUFBRTtjQUNSQyxNQUFNLEVBQUVSLE1BQU0sQ0FBQ0MsUUFBUSxDQUFDRSxHQUFHLENBQUNLLE1BQU07Y0FDbENGLEdBQUcsRUFBRU4sTUFBTSxDQUFDQyxRQUFRLENBQUNFLEdBQUcsQ0FBQ0csR0FBRztjQUM1QkcsSUFBSSxFQUFFVCxNQUFNLENBQUNDLFFBQVEsQ0FBQ0UsR0FBRyxDQUFDTSxJQUFJO2NBQzlCQyxPQUFPLEVBQUVDLElBQUksQ0FBQ0MsS0FBSyxDQUFDRCxJQUFJLENBQUNFLFNBQVMsQ0FBQ2IsTUFBTSxDQUFDQyxRQUFRLENBQUNFLEdBQUcsQ0FBQyxDQUFDLENBQUNPO1lBQzFELENBQUM7WUFDRFQsUUFBUSxFQUFFO2NBQ1RWLElBQUksRUFBRVMsTUFBTSxDQUFDQyxRQUFRLENBQUNWLElBQUk7Y0FDMUJtQixPQUFPLEVBQUVWLE1BQU0sQ0FBQ0MsUUFBUSxDQUFDUyxPQUFPO2NBQ2hDSSxJQUFJLEVBQUVkLE1BQU0sQ0FBQ0MsUUFBUSxDQUFDQztZQUN2QjtVQUNELENBQUMsQ0FBQztRQUNILENBQUMsTUFBTTtVQUNORSxPQUFPLENBQUNDLEtBQUssQ0FBQ0wsTUFBTSxFQUFFVyxJQUFJLENBQUNFLFNBQVMsQ0FBQ2IsTUFBTSxDQUFDLENBQUM7UUFDOUM7TUFDRDtNQUVBLE9BQU9BLE1BQU07SUFDZCxDQUFDLENBQUMsQ0FDRGUsT0FBTyxDQUFDLFlBQU07TUFDZHRCLFFBQVEsQ0FBQ2YsUUFBUSxDQUFDUyxVQUFVLENBQUMsQ0FBQztNQUU5QixJQUFJRSxrQkFBa0IsRUFBRTtRQUN2QkEsa0JBQWtCLENBQUNJLFFBQVEsQ0FBQztNQUM3QjtJQUNELENBQUMsQ0FBQztFQUNKLENBQUM7QUFDRjtBQUFDLGVBRWM7RUFDYnBCLFVBQVUsRUFBVkEsVUFBVTtFQUNWSyxRQUFRLEVBQVJBLFFBQVE7RUFDUk8sY0FBYyxFQUFkQSxjQUFjO0VBQ2RMLGdCQUFnQixFQUFoQkE7QUFDRixDQUFDO0FBQUEifQ==