module.exports = {
	presets: [
		[
			'@babel/preset-env',
			{
				"loose": true,
				"shippedProposals": true,
				"modules": "commonjs",
				"targets": {
					"ie": 9
				}
			}
		],
	],
	plugins: [
		"@babel/plugin-transform-modules-commonjs",
		"@babel/plugin-proposal-class-properties",
		"@babel/plugin-proposal-object-rest-spread",
		"@babel/plugin-proposal-export-default-from",
		"@babel/plugin-proposal-private-methods",
		"@babel/plugin-proposal-private-property-in-object"
	]
};
