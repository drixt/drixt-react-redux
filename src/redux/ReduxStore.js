import {applyMiddleware, combineReducers, compose, createStore} from 'redux';
import thunk from 'redux-thunk';
import BaseReducer from "./BaseReducer";
import {CLEAR_REDUCER_INFO} from "./BaseRATypes";


/**
 * Creates the Redux Store with a default base reducer
 */

const defaultReducers = {
	base: BaseReducer,
};

/**
 * References to store
 */
let wasInit = false;
let reduxStore = null;


/**
 * Utility function that combine reducers enabling global catch of CLEAR_REDUCER_INFO
 * @param asyncReducers
 * @returns {function(*=, *=): *}
 */
const combineReducersWithSugar = (asyncReducers = {}) => {
	return (state, action) => {
		let nextState = state;

		if (action.type === CLEAR_REDUCER_INFO) {
			if (nextState[action.reducer]) {
				nextState = JSON.parse(JSON.stringify(nextState));

				if (!Array.isArray(action.dataTarget))
					action.dataTarget = [action.dataTarget];

				action.dataTarget.forEach(field => {
					nextState[action.reducer][field] = action.cleanValue;
				});
			} else {
				console.error(`[CLEAR_REDUCER_DATA] The given reducer '${action.reducer}' doesn't exists. Current existing reducers are: ${Object.keys(state)}`)
			}
		}

		return combineReducers({...defaultReducers, ...asyncReducers})(nextState, action);
	};
};


/**
 * Utility Function to register Reducers on the fly.
 * @param name
 * @param reducer
 */
const registerLazyReducer = (name, reducer) => {
	if (!reduxStore.asyncReducers[name]) {
		if (process.env.NODE_ENV === 'development')
			console.info(`Registering Lazy Reducer ${name} ...`);

		reduxStore.asyncReducers[name] = reducer;
		reduxStore.replaceReducer(combineReducersWithSugar(reduxStore.asyncReducers));

		if (process.env.NODE_ENV === 'development')
			console.info(`Lazy Reducer ${name} registered!`);
	}
};


/**
 * The principal method of this file.
 * Starts the store.
 *
 * @param middlewares
 * @param enhancers
 * @returns {Store}
 */
const init = (middlewares = [], enhancers = []) => {
	if (!wasInit) {
		const composeEnhancers = (typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;

		//Connect the redux to reactotron console on dev mode
		middlewares.push(thunk);
		reduxStore = createStore(combineReducersWithSugar(),
            composeEnhancers(applyMiddleware(...middlewares), ...enhancers)
		);

		reduxStore.asyncReducers = {};
		wasInit = true;

		if (process.env.NODE_ENV === 'development')
			console.info("ReduxStore: The Store was created!", reduxStore)

		return reduxStore;
	} else {
		if (process.env.NODE_ENV === 'development')
			console.info("ReduxStore: WARN! The Store was already created! Returning the current instance.")

		return reduxStore;
	}
};

const getStore = () => {
	return reduxStore;
}

export default {
	init: init,
	getStore: getStore,
	registerLazyReducer
};
