import {applyMiddleware, combineReducers, compose, legacy_createStore} from 'redux';
import thunk from 'redux-thunk';
import BaseReducer from "./BaseReducer";
import {CLEAR_REDUCER_INFO} from "./BaseRATypes";


/**
 * Creates the Redux Store with a default base reducer
 */

const defaultReducers = {
	base: BaseReducer,
};

/**
 * References to store
 */
let wasInit = false;
let reduxStore = null;


/**
 * Deep clones a JSON state
 * @param currState
 */
export function deepClone(currState) {
	return JSON.parse(JSON.stringify(currState));
}


/**
 * Utility function that combines reducers enabling global catch of CLEAR_REDUCER_INFO
 * @param asyncReducers
 */
function combineReducersWithSugar(asyncReducers = {}) {
	return (state, action) => {
		let nextState = state;

		if (action.type === CLEAR_REDUCER_INFO) {
			if (nextState[action.reducer]) {

				if (!Array.isArray(action.dataTarget)) {
					action.dataTarget = [action.dataTarget];
				}

				action.dataTarget.forEach(field => {
					if (nextState[action.reducer].hasOwnProperty(field)) {
						try {
							nextState[action.reducer][field] = action.cleanValue;
						} catch (e) {
							console.error(`[CLEAR_REDUCER_DATA] The given reducer '${action.reducer}' can't allow state change from the generic 'CLEAR_REDUCER_DATA' action. If you are using immer or something else, maybe you should evict this approach and create custom clean data actions.`)
							console.error(`[CLEAR_REDUCER_DATA]`, e)
						}
					} else {
						console.error(`[CLEAR_REDUCER_DATA] The given reducer '${action.reducer}' don't has a defined property '${field}'. Current existing keys are: ${Object.keys(nextState[action.reducer])}`)
					}
				});
			} else {
				console.error(`[CLEAR_REDUCER_DATA] The given reducer '${action.reducer}' doesn't exists. Current existing reducers are: ${Object.keys(nextState)}`)
			}
		}

		return combineReducers({...defaultReducers, ...asyncReducers})(nextState, action);
	};
}


/**
 * Utility Function to register Reducers on the fly.
 * @param name
 * @param reducer
 */
function registerLazyReducer(name, reducer) {
	if (!reduxStore.asyncReducers[name]) {
		if (process.env.NODE_ENV === 'development')
			console.info(`Registering Lazy Reducer ${name} ...`);

		reduxStore.asyncReducers[name] = reducer;
		reduxStore.replaceReducer(combineReducersWithSugar(reduxStore.asyncReducers));

		if (process.env.NODE_ENV === 'development')
			console.info(`Lazy Reducer ${name} registered!`);
	}
}


/**
 * The principal method of this file.
 * Starts the store.
 *
 * @param middlewares
 * @param enhancers
 */
function init(middlewares = [], enhancers = []) {
	if (!wasInit) {
		const composeEnhancers = (typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;

		//Connect the redux to reactotron console on dev mode
		middlewares.push(thunk);
		reduxStore = legacy_createStore(combineReducersWithSugar(),
			composeEnhancers(applyMiddleware(...middlewares), ...enhancers)
		);

		reduxStore.asyncReducers = {};
		wasInit = true;

		if (process.env.NODE_ENV === 'development')
			console.info("ReduxStore: The Store was created!", reduxStore)

		return reduxStore;
	} else {
		if (process.env.NODE_ENV === 'development')
			console.info("ReduxStore: WARN! The Store was already created! Returning the current instance.")

		return reduxStore;
	}
}

/**
 * Return the current value of reduxStore
 *
 * @returns {reduxStore}
 */
function getStore() {
	return reduxStore;
}

export default {
	init: init,
	getStore: getStore,
	registerLazyReducer
};
