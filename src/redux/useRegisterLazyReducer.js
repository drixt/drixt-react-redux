// Hook
import React, {useEffect, useState} from "react";
import {ReduxStore} from "../index";

/**
 * React Root to Register Lazy Reducers on the Hooks way to provide an easy way to do in-component reducer
 * initialization
 *
 * @param reducerName - The name of reducer
 * @param reducer - The reducer function reference
 * @returns {boolean} - The state of lazy registration.
 */
export default function useRegisterLazyReducer(reducerName, reducer) {
   const [ready, setReady] = useState(false);

   useEffect(() => {
      if (!ReduxStore.getStore().getState()[reducerName]) {
         ReduxStore.registerLazyReducer(reducerName, reducer);

         setTimeout(() => {
            setReady(true);
         }, 100);
      } else {
         setReady(true);
      }
   }, []);

   return ready;
}
