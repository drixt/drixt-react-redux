import {ASYNC_END, ASYNC_START, CLEAR_REDUCER_INFO} from './BaseRATypes'


/**
 * Action creator that start a Async Request Action.
 * @param subtype
 * @param httpRequest
 * @returns {{subtype: string, httpRequest: null, type: string}}
 */
export function asyncStart(subtype = "EMPTY", httpRequest = null) {
	return {
		type: ASYNC_START,
		subtype,
		httpRequest
	};
}


/**
 * Action creator that start a Async Request Action.
 * @param subtype
 * @returns {{subtype: string, type: string}}
 */
export function asyncEnd(subtype = "EMPTY") {
	return {
		type: ASYNC_END,
		subtype,
	};
}


/**
 * Fires a CLEAR action to a specific reducer and reducer value.
 * @param reducer - the reducer name
 * @param dataTarget - field name on reducer, can be a array of field names
 * @param cleanValue - OPTIONAL - value to use on 'cleaning' process.
 * Ex. undefined, 0, "", etc.
 * Defaults to null
 */
export function clearReducerData(reducer, dataTarget, cleanValue = null) {
	return {
		type: CLEAR_REDUCER_INFO,
		reducer,
		dataTarget,
		cleanValue
	};
}


/**
 * Base Async Thunk for common use of async redux actions! ;D
 * @param restApiRequest - The promise returned by the Rest API Path Call
 * @param actionType - Then Redux Action Type to hit Reducer
 * @param customResponseTransform - A function to transform the response body results. Default: returns res.body
 * @param postActionCallback - OPTIONAL: A function to call a custom behavior after the thunk job
 * @returns Function Thunk
 */
export function baseAsyncThunk(restApiRequest,
															 actionType,
															 customResponseTransform = (res, body, hasError) => res.body,
															 postActionCallback = null) {
	return (dispatch) => {
		dispatch(asyncStart(actionType, restApiRequest));

		let hasError = false;
		restApiRequest
			.then(res => {
				hasError = res && !res.body && !res.status.toString().startsWith("20");

				if (!hasError) {
					let body = res.body ? res.body : null;
					dispatch({type: actionType, result: customResponseTransform(res, body, hasError)});
				}

				return res;
			})
			.catch(resErr => {
				hasError = true;

				let body = resErr.response && resErr.response.body ? resErr.response.body : null;

				dispatch({
					type: actionType,
					result: {
						hasError,
						status: resErr.response ? resErr.response.statusCode : 0,
						...customResponseTransform(resErr, body, hasError),
					}
				});

				if (resErr) {
					if (resErr.response && resErr.response.req) {
						console.debug(`baseAsyncThunk XHR: ERROR OCCURRED CALLING >>>> ${resErr.response.req.url}`, {
							request: {
								method: resErr.response.req.method,
								url: resErr.response.req.url,
								data: resErr.response.req.data,
								headers: JSON.parse(JSON.stringify(resErr.response.req)).headers,
							},
							response: {
								body: resErr.response.body,
								headers: resErr.response.headers,
								code: resErr.response.statusCode,
							},
						});
					} else {
						console.debug(resErr, JSON.stringify(resErr));
					}
				}

				return resErr;
			})
			.finally(() => {
				dispatch(asyncEnd(actionType));

				if (postActionCallback) {
					postActionCallback(dispatch);
				}
			});
	};
}

export default {
  asyncStart,
  asyncEnd,
  baseAsyncThunk,
  clearReducerData
}
